CONFIG += console
CONFIG += c++17

TEMPLATE = subdirs

SUBDIRS += \
    Oscilloscope \
    TestDWT \
    TestFFT \
    TestKMeans \
    TestMatrix \
    TestMetrics \
    TestPcmSignal \
    MFCCscope \
    MelBankscope \
    Audioscope
