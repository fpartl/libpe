#include <QtTest>
#include <QVector>
#include <QRandomGenerator>
#include <QDebug>

#include "../../src/frequencyanalysis/fft.h"
#include "../../src/frequencyanalysis/fft.cpp"
#include "../../src/frequencyanalysis/kiss_fft/kiss_fft.h"
#include "../../src/frequencyanalysis/kiss_fft/kiss_fft.c"
#include "../../src/frequencyanalysis/kiss_fft/kiss_fftr.h"
#include "../../src/frequencyanalysis/kiss_fft/kiss_fftr.c"

/* Očekáváná velikost vstupních vekotorů do FFT. */
#define estSegmentSize 30

/* Velikost odhadu výkonové spektrální hodnotu, kterou by měla FFT zvoli při zadání hodnoty estEspdSize */
#define segmentSize 32

/* Počet scénářů při každém testu. */
#define SCENARIOS_COUNT 1

/* Datový typ položek. */
using pcm_sample_t = float;

class TestFFT : public QObject {
    Q_OBJECT

public:
    /*!
     * \brief TestFFT Konstruktor třídy.
     */
    TestFFT();

private slots:
    /*!
     * \brief testFFTInversity Pomocí metody generateVector(int size) vygeneruje segment náhodného signálu,
     *                         který následně "prožene" FFT. Nad výsledkem je následně vypočtena inverze,
     *                         která se porovnává s originálním signálem.
     */
    void testFFTInversity();

    /*!
     * \brief testFFTInversity_data Poskytuje data pro metodu testFFTInversity();
     */
    void testFFTInversity_data();

    /*!
     * \brief testFFTLinearity Metoda testuje výpočet FFT podle předpokladu, že FFT součtu dvou signálu je
     *                         rovna součtu fourierovských obrazů těchto signálů.
     */
    void testFFTLinearity();

    /*!
     * \brief testFFTLinearity_data Poskytuje data pro metodu tetsFFTLinearity().
     */
    void testFFTLinearity_data();

private:
    QRandomGenerator m_generator = QRandomGenerator::securelySeeded();      //!< Generátor náhodných čísel.
    FFT m_fft;                                                              //!< Instance testované třídy.

    /*!
     * \brief generateVector Metoda vygeneruje vektor s náhodnými hodnotami o délce size.
     * \param size Počet prvků ve výsledném vektoru.
     * \return Vygenerovaný vektor délky size.
     */
    QVector<pcm_sample_t> generateVector(int size);
};

TestFFT::TestFFT() : m_fft(estSegmentSize) { }

void TestFFT::testFFTInversity() {
    QFETCH(QVector<pcm_sample_t>, signal);

    Q_ASSERT(signal.size() == segmentSize);

    QVector<kiss_fft_cpx> spectrum = m_fft.realFFt(signal);
    QVector<float> inverse = m_fft.invRealFFT(spectrum);

    QCOMPARE(spectrum.size(), (segmentSize / 2) + 1);    // pro kontrolu (mělo by to být n/2 + 1)
    QCOMPARE(spectrum.size(), m_fft.espdSize());

    QCOMPARE(inverse.size(), segmentSize);
    QCOMPARE(inverse.size(), m_fft.maxInputSize());

    /* Přímá komparace tady nemá význam protože dochází k mírnému zašumění signálů. */
    qDebug() << "original: " << signal;
    qDebug() << "inverze: " << inverse;
}

void TestFFT::testFFTInversity_data() {
    QTest::addColumn<QVector<pcm_sample_t>>("signal");

    QTest::newRow(QString("testFFT").toLocal8Bit().constData()) << generateVector(segmentSize);

}

void TestFFT::testFFTLinearity() {
    QFETCH(QVector<pcm_sample_t>, signal);
    QFETCH(QVector<pcm_sample_t>, signal2);

    Q_ASSERT(signal.size() == segmentSize);
    Q_ASSERT(signal2.size() == segmentSize);

    QVector<pcm_sample_t> signalsSum;
    for (int i = 0; i < signal.size(); i++)
        signalsSum.append(signal.at(i) + signal2.at(i));

    Q_ASSERT(signalsSum.size() == segmentSize);

    QVector<kiss_fft_cpx> spectrumOfSignalSum = m_fft.realFFt(signalsSum);

    QVector<kiss_fft_cpx> spectrumOfSignal = m_fft.realFFt(signal);
    QVector<kiss_fft_cpx> spectrumOfSignal2 = m_fft.realFFt(signal2);

    QVector<kiss_fft_cpx> spectersSum;
    for (int i = 0; i < spectrumOfSignal.size(); i++) {
        kiss_fft_cpx c;

        c.r = spectrumOfSignal[i].r + spectrumOfSignal2[i].r;
        c.i = spectrumOfSignal[i].i + spectrumOfSignal2[i].i;

        spectersSum.append(c);
    }

    for (int i = 0; i < spectrumOfSignalSum.size(); i++) {
        qDebug() << "spectrumOfSignalSum[i].r = " << spectrumOfSignalSum[i].r << ", spectersSum[i].r = " << spectersSum[i].r;
        qDebug() << "spectrumOfSignalSum[i].r = " << spectrumOfSignalSum[i].i << ", spectersSum[i].r = " << spectersSum[i].r;
    }
}

void TestFFT::testFFTLinearity_data() {
    QTest::addColumn<QVector<pcm_sample_t>>("signal");
    QTest::addColumn<QVector<pcm_sample_t>>("signal2");

    for (int i = 0; i < SCENARIOS_COUNT; i++) {
        QTest::newRow(QString("testFFT %1").arg(i).toLocal8Bit().constData())
                << generateVector(segmentSize) << generateVector(segmentSize);
    }
}

QVector<pcm_sample_t> TestFFT::generateVector(int size) {
    QVector<pcm_sample_t> vector;
    vector.resize(size);

    Q_ASSERT(vector.size() == size);

    for (int i = 0; i < vector.size(); i++)
        vector[i] = static_cast<pcm_sample_t>(m_generator.generate());

    return vector;
}


QTEST_APPLESS_MAIN(TestFFT)
#include "tst_testfft.moc"
