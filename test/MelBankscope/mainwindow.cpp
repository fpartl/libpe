#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      m_bank(ESPD_SIZE, FILTERS_COUNT, SAMPLE_RATE)
{
    ui->setupUi(this);
    this->setWindowTitle("Banka melovských filtrů.");

    initChart();
    startMelBankscope();
}

MainWindow::~MainWindow() {
    delete ui;
    delete m_chartView;
}

void MainWindow::initChart() {
    QtCharts::QChart *chart = new QtCharts::QChart();
    chart->legend()->hide();
    chart->setTitle("Banka melovských filtrů.");

    m_chartView = new QtCharts::QChartView(chart);
    m_chartView->setRenderHint(QPainter::Antialiasing);

    this->setCentralWidget(m_chartView);
    this->resize(1600, 800);
}

void MainWindow::startMelBankscope() {
    for (int i = 0; i < m_bank.size(); i++) {
        QVector<float> filter = m_bank.getFilter(i);

        QtCharts::QLineSeries *series = new QtCharts::QLineSeries;
        for (int j = 0; j < filter.size(); j++)
            series->append(static_cast<double>(j), static_cast<double>(filter[j]));

        m_chartView->chart()->addSeries(series);
        m_chartView->chart()->createDefaultAxes();
    }
}
