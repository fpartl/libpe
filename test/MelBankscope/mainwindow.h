#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QMainWindow>

#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QValueAxis>

#include "../../src/mfcc/melfilterbank.h"

constexpr int ESPD_SIZE = 2048;
constexpr int FILTERS_COUNT = 20;
constexpr int SAMPLE_RATE = 44100;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QtCharts::QChartView *m_chartView;

    MelFilterBank m_bank;

    /*!
     * \brief initChart Metoda provede iniciliaziaci grafu, kde bude vše vykresleno.
     */
    void initChart();

    /*!
     * \brief startMelBankscope Metoda připraví objekt m_segmenter a spustí nahrávání.
     */
    void startMelBankscope();
};

#endif
