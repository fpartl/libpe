#include <QtTest>
#include <QRandomGenerator>

#include "../../src/containers/matrix.h"

Q_DECLARE_METATYPE(Matrix<double>);

class TestMatrix : public QObject {
    Q_OBJECT

private slots:
    void testDimensions();

    void testDimensions_data();

    void testEquality();

    void testEquality_data();

private:
    QRandomGenerator m_generator = QRandomGenerator::securelySeeded();    //!< Generátor náhodných čísel.

    /*!
     * \brief SCENARIOS_COUNT Počet scénářů při každém testu.
     */
    static constexpr int SCENARIOS_COUNT = 32;

    /*!
     * \brief MAX_TEST_VECTOR_SIZE Maximální velikosti generovaných testovacích matic.
     */
    static constexpr int MAX_TEST_VECTOR_SIZE = 256;

    Matrix<double> generateMatrix(int rows = 0, int cols = 0, double val = 0.0);
};

void TestMatrix::testDimensions() {
    QFETCH(Matrix<double>, mat);
    QFETCH(int, rows);
    QFETCH(int, cols);

    QCOMPARE(mat.rows(), rows);
    QCOMPARE(mat.cols(), cols);
}

void TestMatrix::testDimensions_data() {
    QTest::addColumn<Matrix<double>>("mat");
    QTest::addColumn<int>("rows");
    QTest::addColumn<int>("cols");

    for (int i = 0; i < SCENARIOS_COUNT; ++i) {
        int rows = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1;
        int cols = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1;

        Matrix<double> mat(rows, cols);

        QTest::newRow(QString("Dimensions %1").arg(i).toLocal8Bit().constData()) << mat << rows << cols;
    }
}

void TestMatrix::testEquality() {
    QFETCH(Matrix<double>, mat1);
    QFETCH(Matrix<double>, mat2);
    QFETCH(bool, equal);

    QCOMPARE(mat1 == mat2, equal);
    QCOMPARE(!(mat1 != mat2), equal);
}

void TestMatrix::testEquality_data() {
    QTest::addColumn<Matrix<double>>("mat1");
    QTest::addColumn<Matrix<double>>("mat2");
    QTest::addColumn<bool>("equal");

    for (int i = 0; i < SCENARIOS_COUNT; ++i) {
        if (i % 5 == 0) {
            Matrix<double> mat = generateMatrix();

            QTest::newRow(QString("Equality %1").arg(i).toLocal8Bit().constData()) << mat << mat << true;
        }
        else if (i % 3 == 0) {
            Matrix<double> mat1 = generateMatrix();
            Matrix<double> mat2 = generateMatrix(mat1.rows() + 1);

            QTest::newRow(QString("Equality %1").arg(i).toLocal8Bit().constData()) << mat1 << mat2 << false;
        }
        else {
            Matrix<double> mat1 = generateMatrix();
            Matrix<double> mat2 = generateMatrix(mat1.rows(), mat1.cols(), mat1[0][0] + 1.0);

            QTest::newRow(QString("Equality %1").arg(i).toLocal8Bit().constData()) << mat1 << mat2 << false;
        }
    }
}

Matrix<double> TestMatrix::generateMatrix(int rows, int cols, double val) {
    if (rows <= 0)
        rows = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1;

    if (cols <= 0)
        cols = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1;

    if (val <= 0)
        val = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1;

    Matrix<double> mat(rows, cols, val);
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            mat[i][j] = m_generator.generateDouble();
        }
    }

    return mat;
}

QTEST_APPLESS_MAIN(TestMatrix)

#include "tst_testmatrix.moc"
