#include <QtTest>
#include <QVector>
#include <QRandomGenerator>

#include "../../src/metrics/metrics.h"

class TestMetrics : public QObject {
    Q_OBJECT

private slots:
    void testIdenticalPoints();

    void testIdenticalPoints_data();

    void testDiffPoints();

    void testDiffPoints_data();

    void testDiffValues();

    void testDiffValues_data();

private:
    QRandomGenerator m_generator = QRandomGenerator::securelySeeded();

    /*!
     * \brief SCENARIOS_COUNT Počet scénářů při každém testu.
     */
    static constexpr int SCENARIOS_COUNT = 32;

    /*!
     * \brief MAX_TEST_VECTOR_SIZE Maximální velikosti generovaných testovacích matic.
     */
    static constexpr int MAX_TEST_VECTOR_SIZE = 256;

    /*!
     * \brief generateVector Metoda vygeneruje vektor s náhodnými hodnotami o délce size.
     * \param size Počet prvků ve výsledném vektoru.
     * \return Vygenerovaný vektor délky size.
     */
    QVector<double> generateVector(int size = -1);
};

void TestMetrics::testIdenticalPoints() {
    QFETCH(QVector<double>, v1);
    QFETCH(QVector<double>, v2);

    QCOMPARE(Metrics::euclDistN(v1, v2), 0.0);
}

void TestMetrics::testIdenticalPoints_data() {
    QTest::addColumn<QVector<double>>("v1");
    QTest::addColumn<QVector<double>>("v2");

    for (int i = 0; i < SCENARIOS_COUNT; ++i) {
        QVector<double> vec = generateVector();

        QTest::newRow(QString("identicalPoints %1").arg(i).toLocal8Bit().constData())
            << vec << vec;
    }
}

void TestMetrics::testDiffPoints() {
    QFETCH(QVector<double>, v1);
    QFETCH(QVector<double>, v2);
    QFETCH(double, dist);

    QCOMPARE(Metrics::euclDistN(v1, v2), dist);
}

void TestMetrics::testDiffPoints_data() {
    QTest::addColumn<QVector<double>>("v1");
    QTest::addColumn<QVector<double>>("v2");
    QTest::addColumn<double>("dist");

    for (int i = 0; i < SCENARIOS_COUNT; ++i) {
        QVector<double> v1 = generateVector();
        QVector<double> v2 = generateVector(v1.size());
        double dist = 0.0;

        for (int i = 0; i < v1.size(); ++i)
            dist += std::pow(v1[i] - v2[i], 2);

        QTest::newRow(QString("diffPoints %1").arg(i).toLocal8Bit().constData())
            << v1 << v2 << std::sqrt(dist);
    }
}

void TestMetrics::testDiffValues() {
    QFETCH(double, x);
    QFETCH(double, y);

    QVector<double> vec_x;  vec_x << x;
    QVector<double> vec_y;  vec_y << y;

    QCOMPARE(Metrics::euclDist(x, y), Metrics::euclDistN(vec_x, vec_y));
}

void TestMetrics::testDiffValues_data() {
    QTest::addColumn<double>("x");
    QTest::addColumn<double>("y");

    for (int i = 0; i < SCENARIOS_COUNT; ++i) {
        QTest::newRow(QString("values %1").arg(i).toLocal8Bit().constData())
            << m_generator.generateDouble()
            << m_generator.generateDouble();
    }
}

QVector<double> TestMetrics::generateVector(int size) {
    if (size < 0)
        size = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1;

    QVector<double> result;
    result.reserve(size);

    for (int i = 0; i < size; ++i)
        result.append(m_generator.generateDouble());

    return result;
}

QTEST_APPLESS_MAIN(TestMetrics)

#include "tst_testmetrics.moc"
