#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QMainWindow>
#include <QAudioInput>
#include <QAudioFormat>
#include <QDebug>
#include <limits>

#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QValueAxis>

#include "../../src/segmentation/iosignalsegmenter.h"
#include "../../src/windowfunctions/hammingwindow.h"
#include "../../src/frequencyanalysis/fft.h"

using sample = short;
constexpr int SAMPLE_RATE = 44100;
constexpr int CHANNEL_COUNT = 1;
constexpr auto CODEC = "audio/pcm";
constexpr int SEGMENT_SIZE = 256;
constexpr int OVERLAP = 0;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QtCharts::QChartView m_chartView;
    QtCharts::QValueAxis axisX;
    QtCharts::QValueAxis axisY;

    QAudioInput m_audioInput;
    IOSignalSegmenter<sample> m_segmenter;
    HammingWindow<sample> m_windowFunction;

    /*!
     * \brief initAudioFormat Metode provede inicializaci nastavení záznamu signálu.
     * \return Nastavení záznamu signálu.
     */
    QAudioFormat initAudioFormat();

    /*!
     * \brief initChart Metoda provede iniciliaziaci grafu, kde bude vše vykresleno.
     */
    void initChart();

    /*!
     * \brief startOscilloscope Metoda připraví objekt m_segmenter a spustí nahrávání.
     */
    void startAudioScope();

    /*!
     * \brief drawLineGraph Metoda provede vykreslení zadaného vektoru hodnot do grafu m_chart.
     */
    void drawLineGraph(const QVector<float> &data);

private slots:
    void processSegment(QVector<sample> segment);
};

#endif
