#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      m_audioInput(QAudioDeviceInfo::defaultInputDevice(), initAudioFormat()),
      m_segmenter(SEGMENT_SIZE, OVERLAP),
      m_windowFunction(SEGMENT_SIZE)
{
    ui->setupUi(this);
    this->setWindowTitle("Audioskop");

    initChart();
    startAudioScope();
}

MainWindow::~MainWindow() {
    m_audioInput.stop();

    delete ui;
}

QAudioFormat MainWindow::initAudioFormat() {
    QAudioFormat formatAudio;

    formatAudio.setSampleRate(SAMPLE_RATE);
    formatAudio.setChannelCount(CHANNEL_COUNT);
    formatAudio.setSampleSize(sizeof(sample) * 8);
    formatAudio.setCodec(CODEC);
    formatAudio.setByteOrder(QAudioFormat::LittleEndian);
    formatAudio.setSampleType(QAudioFormat::SignedInt);

    return formatAudio;
}

void MainWindow::initChart() {
    QtCharts::QChart *chart = new QtCharts::QChart();
    chart->legend()->hide();
    chart->setTitle("Normalizovaný segment akustického signálu");

    m_chartView.setChart(chart);
    m_chartView.setRenderHint(QPainter::Antialiasing);

    this->setCentralWidget(&m_chartView);
    this->resize(1600, 800);

    axisX.setLabelFormat("%d");
    axisX.setTickCount(7);
    axisX.setRange(0, m_segmenter.segmentSize());
    axisX.setTitleText("Čas [biny]");
    m_chartView.chart()->addAxis(&axisX, Qt::AlignBottom);

    axisY.setLabelFormat("%d");
    axisY.setTickCount(7);
    axisY.setRange(std::numeric_limits<short>::min() + 1, std::numeric_limits<short>::max());
    axisY.setTitleText("Amplituda [abs.]");
    m_chartView.chart()->addAxis(&axisY, Qt::AlignLeft);
}

void MainWindow::startAudioScope() {
    m_segmenter.open(QIODevice::WriteOnly);
    QObject::connect(&m_segmenter, QOverload<QVector<sample>>::of(&IOSignalSegmenter<sample>::segmentReady), this, &MainWindow::processSegment);

    m_audioInput.start(&m_segmenter);
}

void MainWindow::drawLineGraph(const QVector<float> &data) {
    QtCharts::QLineSeries *series = new QtCharts::QLineSeries;
    for (int i = 0; i < data.size(); i++)
        series->append(static_cast<double>(i), static_cast<double>(data[i]));

    m_chartView.chart()->removeAllSeries();
    m_chartView.chart()->addSeries(series);

    series->attachAxis(&axisX);
    series->attachAxis(&axisY);
}

void MainWindow::processSegment(QVector<sample> segment) {
    QVector<float> weighted = m_windowFunction.normalize(segment);

    drawLineGraph(weighted);
    //m_chartView->chart()->createDefaultAxes();
}

