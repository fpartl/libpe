#include <QtTest>
#include <QVector>
#include <QRandomGenerator>

#include "../../src/containers/pcmsignal.h"

/* Datový typ položek. */
using pcm_sample_t = int;

/*!
 * \brief Třída TestPcmSignal
 *
 * Unit test třídy PcmSignal.
 */
class TestPcmSignal : public QObject {
    Q_OBJECT


private slots:
    /*!
     * \brief testInvalid Metoda zkontroluje chování metody PcmSignal<T>::invalid()
     *                    a PcmSignal<T>::isValid().
     */
    void testIsValid();

    /*!
     * \brief testInvalid_data Metoda poskytuje data pro metodu testInvalid().
     */
    void testIsValid_data();

    /*!
     * \brief testGetPcm Metoda zkontroluje chování metody PcmSignal<T>::getPcm().
     */
    void testGetPcmSampleRate();

    /*!
     * \brief testGetPcm_data Metoda poskytuje data pro metodu testGetPcm().
     */
    void testGetPcmSampleRate_data();

    /*!
     * \brief testGetDuration Metoda zkontroluje chování metody PcmSignal<T>::getDuration().
     */
    void testGetDuration();

    /*!
     * \brief testGetDuration_data Metoda poskytuje data pro metodu getDuration();
     */
    void testGetDuration_data();

private:
    QRandomGenerator m_generator = QRandomGenerator::securelySeeded();    //!< Generátor náhodných čísel.

    /*!
     * \brief SCENARIOS_COUNT Počet scénářů při každém testu.
     */
    static constexpr int SCENARIOS_COUNT = 32;

    /*!
     * \brief MAX_TEST_VECTOR_SIZE Maximální velikosti generovaných testovacích vektorů.
     */
    static constexpr int MAX_TEST_VECTOR_SIZE = 256;

    /*!
     * \brief generateVector Metoda vygeneruje vektor s náhodnými hodnotami o délce size. Pokud je size nastaven
     *                       na nula nebo hodnotě menší, délka vektoru bude taktéž náhodná.
     * \param size Počet prvků ve výsledném vektoru.
     * \return Vygenerovaný vektor délky size nebo náhodné pokud je size <= 0.
     */
    QVector<pcm_sample_t> generateVector(int size = 0);
};

void TestPcmSignal::testIsValid() {
    QFETCH(QVector<pcm_sample_t>, signal);
    QFETCH(int, sampleRate);
    QFETCH(bool, valid);

    PcmSignal<pcm_sample_t> pcm(signal, sampleRate);

    QCOMPARE(pcm.isValid(), valid);
}

void TestPcmSignal::testIsValid_data() {
    QTest::addColumn<QVector<pcm_sample_t>>("signal");
    QTest::addColumn<int>("sampleRate");
    QTest::addColumn<bool>("valid");

    for (int i = 0; i < SCENARIOS_COUNT; i++) {
        QVector<pcm_sample_t> s = generateVector();
        int sR = static_cast<int>(m_generator.generate());
        bool valid = !s.isEmpty() && sR > 0;

        QTest::newRow(QString("isValid %1").arg(i).toLocal8Bit().constData()) << s << sR << valid;
    }
}

void TestPcmSignal::testGetPcmSampleRate() {
    QFETCH(QVector<pcm_sample_t>, signal);
    QFETCH(int, sampleRate);

    PcmSignal<pcm_sample_t> pcm(signal, sampleRate);

    if (pcm.isValid()) {
        QCOMPARE(pcm.getPcm(), signal);
        QCOMPARE(pcm.getSampleRate(), sampleRate);
    }
    else {
        QCOMPARE(pcm.getPcm(), QVector<pcm_sample_t>());
        QCOMPARE(pcm.getSampleRate(), 0);
    }
}

void TestPcmSignal::testGetPcmSampleRate_data() {
    QTest::addColumn<QVector<pcm_sample_t>>("signal");
    QTest::addColumn<int>("sampleRate");

    for (int i = 0; i < SCENARIOS_COUNT; i++)
        QTest::newRow(QString("isValid %1").arg(i).toLocal8Bit().constData()) << generateVector() << static_cast<int>(m_generator.generate());
}

void TestPcmSignal::testGetDuration() {
    QFETCH(QVector<pcm_sample_t>, signal);
    QFETCH(int, sampleRate);
    QFETCH(int, duration);

    PcmSignal<pcm_sample_t> pcm(signal, sampleRate);

    if (pcm.isValid()) {
        QCOMPARE(pcm.getDuration(), duration);
    }
    else {
        QCOMPARE(pcm.getDuration(), 0);
    }
}

void TestPcmSignal::testGetDuration_data() {
    QTest::addColumn<QVector<pcm_sample_t>>("signal");
    QTest::addColumn<int>("sampleRate");
    QTest::addColumn<int>("duration");

    for (int i = 0; i < SCENARIOS_COUNT; i++) {
        QVector<pcm_sample_t> s = generateVector();
        int sR = static_cast<int>(m_generator.generate());

        int duration = (!s.isEmpty() && sR > 0)
                            ? (s.size() * 1000) / sR
                            : 0;

        QTest::newRow(QString("isValid %1").arg(i).toLocal8Bit().constData()) << s << sR << duration;
    }
}

QVector<pcm_sample_t> TestPcmSignal::generateVector(int size) {
    if (size <= 0)
        size = m_generator.bounded(MAX_TEST_VECTOR_SIZE); // -1 +1, aby byl vektor vždy alespoň jednoprvkový

    QVector<pcm_sample_t> vector;
    vector.resize(size);

    Q_ASSERT(vector.size() == size);

    for (int i = 0; i < vector.size(); i++)
        vector[i] = static_cast<pcm_sample_t>(m_generator.generate());

    return vector;
}

QTEST_APPLESS_MAIN(TestPcmSignal)
#include "tst_testpcmsignal.moc"
