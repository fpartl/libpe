#include <QtTest>
#include <QVector>

#include "../../src/clustering/kmeans.h"

class TestKMeans : public QObject {
    Q_OBJECT

private slots:
    void testKnownExample();

    void testKnownExampleN();
};

void TestKMeans::testKnownExample() {
    QVector<QVector<double>> data = {
        { 1.0 }, { 2.0 }, { 3.0 }, { 10.0 }, { 11.0 }, { 12.0 }
    };

    KMeans<double> kmean;
    KMeans<double>::Result result = kmean.fitpp(data, 2); //kmean.fit(data, centroids);

    qDebug() << "Popisy shluku:";
    for (auto cl : result.clusters) {
        qDebug() << "Vzdalenosti: " << cl.totalDistance;
        qDebug() << "Centroid:    " << cl.centroid;
        qDebug() << "Body:        ";

        qDebug() << "test " << cl.points.size();

        for (auto point : cl.points)
            qDebug() << *point;
    }

    qDebug() << "Vzdalenost celkova:  " << result.totalDistance;
    qDebug() << "Pocet iteraci:       " << result.iterations;

    double distance;
    auto targetCluster = kmean.clustrify(result, { 13.0 }, &distance);

    qDebug() << "Přiřazený centroid: " << targetCluster->centroid;
    qDebug() << "Vzdálenost k centroidu: " << distance;

    QVector<KMeans<double>::Point> centroids;
    for (const auto& cls : result.clusters)
        centroids.append(cls.centroid);

    auto centroid = kmean.clustrify(centroids, { 14.0 }, &distance);
    qDebug() << "Přiřazený centroid: " << centroid;
    qDebug() << "Vzdálenost k centroidu: " << distance;
}

void TestKMeans::testKnownExampleN() {
    QVector<QVector<double>> data = {
        { 1.0, 1.0 }, { 2.0, 1.0 }, { 3.0, 1.0 }, { 10.0, 10.0 }, { 11.0, 10.0 }, { 12.0, 10.0 }
    };

    KMeans<double> kmean;
    KMeans<double>::Result result = kmean.fitpp(data, 2); //kmean.fit(data, centroids);

    qDebug() << "Popisy shluku:";
    for (auto cl : result.clusters) {
        qDebug() << "Vzdalenosti: " << cl.totalDistance;
        qDebug() << "Centroid:    " << cl.centroid;
        qDebug() << "Body:        ";

        qDebug() << "test " << cl.points.size();

        for (auto point : cl.points)
            qDebug() << *point;
    }

    qDebug() << "Vzdalenost celkova:  " << result.totalDistance;
    qDebug() << "Pocet iteraci:       " << result.iterations;

    double distance;
    auto targetCluster = kmean.clustrify(result, { 13.0, 10.0 }, &distance);

    qDebug() << "Přiřazený centroid: " << targetCluster->centroid;
    qDebug() << "Vzdálenost k centroidu: " << distance;

    QVector<KMeans<double>::Point> centroids;
    for (const auto& cls : result.clusters)
        centroids.append(cls.centroid);

    auto centroid = kmean.clustrify(centroids, { 14.0, 10.0 }, &distance);
    qDebug() << "Přiřazený centroid: " << centroid;
    qDebug() << "Vzdálenost k centroidu: " << distance;
}


QTEST_APPLESS_MAIN(TestKMeans)
#include "tst_testkmeans.moc"
