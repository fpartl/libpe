#include <QtTest>
#include <limits>

#include <iostream>

#include "../../src/containers/matrix.h"
#include "../../src/matching/dtw.h"

class TestDWT : public QObject {
    Q_OBJECT

public:
    TestDWT();

private:
    DTW<int> m_dtw;

private slots:
    void test_zeroSignals();

    void test_knownSample();

    void test_knownSample2();
};

TestDWT::TestDWT()
    : m_dtw([](int a, int b) -> double {
          return static_cast<double>(std::abs(a - b));
      })
{ }

void TestDWT::test_zeroSignals() {
    QVector<int> s1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    QVector<int> s2 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    QCOMPARE(m_dtw.calcWarpCost(s1, s2), 0.0);
}

void TestDWT::test_knownSample() {
    QVector<int> s1 = { 2, 4, 1, 3 };
    QVector<int> s2 = { 1, 2, 4, 3, 7 };

    Matrix<double> expDistMat(s1.size(), s2.size(), 0.0);
    expDistMat[0] = { 1.0, 1.0, 3.0, 4.0, 9.0 };
    expDistMat[1] = { 4.0, 3.0, 1.0, 2.0, 5.0 };
    expDistMat[2] = { 4.0, 4.0, 4.0, 3.0, 8.0 };
    expDistMat[3] = { 6.0, 5.0, 5.0, 3.0, 7.0 };

    QVERIFY2(expDistMat == m_dtw.calcWarpMat(s1, s2), "Matice nejsou stejné...");
    QCOMPARE(m_dtw.calcWarpCost(s1, s2), 7.0);
}

void TestDWT::test_knownSample2() {
    QVector<int> s1 = { 3, 1, 2, 4, 1 };
    QVector<int> s2 = { 2, 3, 5, 2, 1, 2 };

    Matrix<double> expDistMat(s1.size(), s2.size(), 0.0);
    expDistMat[0] = { 1.0, 1.0, 3.0, 4.0, 6.0, 7.0 };
    expDistMat[1] = { 2.0, 3.0, 5.0, 4.0, 4.0, 5.0 };
    expDistMat[2] = { 2.0, 3.0, 6.0, 4.0, 5.0, 4.0 };
    expDistMat[3] = { 4.0, 3.0, 4.0, 6.0, 7.0, 6.0 };
    expDistMat[4] = { 5.0, 5.0, 7.0, 5.0, 5.0, 6.0 };

    std::cout << expDistMat;
    std::cout << m_dtw.calcWarpMat(s1, s2);

    QVERIFY2(expDistMat == m_dtw.calcWarpMat(s1, s2), "Matice nejsou stejné...");
    QCOMPARE(m_dtw.calcWarpCost(s1, s2), 6.0);
}

QTEST_APPLESS_MAIN(TestDWT)

#include "tst_testdwt.moc"
