QT       += core gui multimedia charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../../src/frequencyanalysis/fft.cpp \
    ../../src/frequencyanalysis/kiss_fft/kiss_fft.c \
    ../../src/frequencyanalysis/kiss_fft/kiss_fftr.c \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../../src/frequencyanalysis/fft.h \
    ../../src/frequencyanalysis/kiss_fft/_kiss_fft_guts.h \
    ../../src/frequencyanalysis/kiss_fft/kiss_fft.h \
    ../../src/frequencyanalysis/kiss_fft/kiss_fftr.h \
    ../../src/segmentation/iosignalsegmenter.h \
    ../../src/segmentation/signalsegmenter.h \
    ../../src/windowfunctions/hammingwindow.h \
    ../../src/windowfunctions/windowfunction.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Adresář, kde se očekávají binární soubory užitých knihoven. Pro jejich sestavení
# je třeba správně nakonfigurovat a spustit skript deps/buildDeps.sh.
LIBS_BUILD_DIR = $$PWD/../../deps


# Adresář se sestavenými knihovnami FFmpeg, OpenCV a Dlib.
LIBFP_BUILD_DIR = $$LIBS_BUILD_DIR/libfp

INCLUDEPATH += \
    $$LIBFP_BUILD_DIR/include


# Načtení umístění binárních souborů knihoven.
LIBS += \
    $$LIBFP_BUILD_DIR/build/linux_x64/liblibfp.a


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
