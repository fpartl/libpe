#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <QVector>

/*!
 * \brief Třída Matrix
 *
 * Představuje matici (matematický objekt) s prvky danými typem T (jedná se o šablonovanou třídu).
 * Matice je implementována jako vektor vektorů (řádků matice). V matici lze snadno indexovat pomocí
 * indexů i (řádek) a j (sloupec). Oba indexy jsou číslovány od nuly. Po konstrukci již nelze měnit
 * rozměry matice.
 */
template <class T>
class Matrix {
public:
    explicit Matrix() = default; // Defaultní konstruktor není povolený, ale je nutný v unit testech.

    /*!
     * \brief Matrix Konstruktor třídy. Alokuje matici s rozměry rows*cols, kde prvky budou nastaveny
     *               na hodnotu val. Pokud tato hodnota nebude stanovena, použije se výchozí hodnota
     *               daného typu prvků matice.
     * \param rows Počet řádků matice.
     * \param cols Počet sloupců matice.
     * \param val Výchozí hodnota prvků matice.
     */
    explicit Matrix(int rows, int cols, T val = T());

    /*!
     * \brief fill Metoda provede nastavení všech prvků matice na danou hodnotu val.
     * \param val Hodnota, na kterou budou nastaveny všechny prvky matice.
     */
    void fill(T val);

    /*!
     * \brief rows Metoda vrátí počet řádků matice.
     * \return Počet řádků matice
     */
    int rows() const;

    /*!
     * \brief cols Metoda vrátí počet sloupců matice.
     * \return Počet sloupců matice.
     */
    int cols() const;

    /*!
     * \brief operator[] Přetížený operátor, kterým lze získat i-tý řádek matice, tj. referenci na konkrétní
     *                   řádek matice. V tomto přetížení se konkrétně jedná o nekonstantní referenci.
     * \param i Index požadovaného řádku matice.
     * \return Nekonstatní reference na řádek matice (objekt třídy QVector<T>).
     */
    QVector<T>& operator[](int i);

    /*!
     * \brief operator[] Přetížený operátor, kterým lze získat i-tý řádek matice, tj. referenci na konkrétní
     *                   řádek matice. V tomto přetížení se jedná o konstatní referenci, jejíž použití může
     *                   výrazně zvýšit výkon algoritmu.
     * \param i Index požadovaného řádku matice.
     * \return Konstantní reference na řádek matice (objekt třídy QVector<T>).
     */
    const QVector<T>& operator[](int i) const;

    /*!
     * \brief operator== Přetížení operátoru ==. Vrací true, pokud jsou matice stejných rozměrů a obsahují
     *                   prvky stejných hodnot.
     * \param mat Matice, se kterou se bude tato matice porovnávat.
     * \return True, pokud jsou matice shodné z hlediska rozměrů i hodnot prvků.
     */
    bool operator==(const Matrix<T>& mat) const;

    /*!
     * \brief operator!= Operatár, který vrací negaci výsledku operace ==.
     * \param mat Matice, se kterou se bude tato matice porovnávat.
     * \return True, pokud jsou matice neshodné z hlediska rozměrů nebo hodnot prvků.
     */
    bool operator!=(const Matrix<T>& mat) const;

private:
    QVector<QVector<T>> m_data;     //!< Položky matice.

    /*!
     * \brief operator<< Přetížení operátoru bitového posunu pro výpis obsahu matice do standartního výstupu.
     * \param stream Proud, do kterého budou data matice vytisknuta.
     * \param mat Matice, jejíž položky budou vytisknuty.
     * \return Reference na výstupní proud, aby bylo možné je řetězit.
     */
    template <typename U>
    friend std::ostream& operator<<(std::ostream& stream, const Matrix<U>& mat);
};

template<class T>
Matrix<T>::Matrix(int rows, int cols, T val)
    : m_data(QVector<QVector<T>>(rows, QVector<T>(cols, val))) { }

template<class T>
void Matrix<T>::fill(T val) {
    for (QVector<T>& vec : m_data)
        vec.fill(val);
}

template<class T>
int Matrix<T>::rows() const {
    return m_data.size();
}

template<class T>
int Matrix<T>::cols() const {
    return m_data.first().size();
}

template<class T>
QVector<T>& Matrix<T>::operator[](int i) {
    Q_ASSERT(i >= 0 && i < m_data.size());

    return m_data[i];
}

template<class T>
const QVector<T>& Matrix<T>::operator[](int i) const {
    Q_ASSERT(i >= 0 && i < m_data.size());

    return m_data[i];
}

template<class T>
bool Matrix<T>::operator==(const Matrix<T> &mat) const {
    if (this->rows() != mat.rows() || this->cols() != mat.cols())
        return false;

    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            if ((*this)[i][j] != mat[i][j])
                return false;
        }
    }

    return true;
}

template<class T>
bool Matrix<T>::operator!=(const Matrix<T> &mat) const {
    return !(*this == mat);
}

template<class T>
std::ostream& operator<<(std::ostream& stream, const Matrix<T>& mat) {
    for (const QVector<T>& row : mat.m_data) {
        stream << "| ";

        for (const T& i : row)
            stream << i << " ";

        stream << "|" << std::endl;
    }

    return stream << std::endl;
}

#endif
