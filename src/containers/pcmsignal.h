#ifndef PCMSIGNAL_H
#define PCMSIGNAL_H

#include <QObject>
#include <QVector>

/*!
 * \brief Třída PcmAudio
 *
 * Třída představuje audiozáznam vzorkovaný metodou PCM. Jedná se kontejner, který spravuje
 * samotné vzorky akustického signálu a vzorkovací frekvenci, se kterou byly vzorky pořízeny.
 */
template <class T>
class PcmSignal {
public:
    /*!
     * \brief PcmAudio Konstruktor třídy.
     * \param pcm Vektor obsahující vzorky akustického signálu.
     * \param sampleRate Vzorkovací frekcence akustického signálu.
     */
    explicit PcmSignal(const QVector<T> &pcm, int sampleRate);

    /*!
     * \brief invalid Statická metoda, která zkonstruuje objekt třídy, který nebude validní.
     *                Jeho metoda isValid() bude vracet hodnotu false.
     * \return Nevalidní objekt třídy PcmAudio.
     */
    static PcmSignal invalid();

    /*!
     * \brief getPcm Getter, který vrací vzorky akustického signálu. Pokud je objekt v nevalidním
     *               stavu, metoda vrátí prázdný vektor.
     * \return Vzorky akustického signálu.
     */
    QVector<T> getPcm() const;

    /*!
     * \brief getSampleRate Getter, který vrací vzorkovací frekvenci akustického signálu.
     *                      Pokud je objekt v nevalidním stavu, metoda vrátí hodnotu 0.
     * \return Vzorkovací frekvence akustického signálu.
     */
    int getSampleRate() const;

    /*!
     * \brief getDuration Metoda, která dle vzorkovací frekvence a počtu vzorků akustického
     *                    signálu určuje jeho časovou délku. Pokud je objekt v nevalidním stavu, metoda
     *                    vrátí hodnotu 0.
     * \return Délka audio záznamu v milisekundách.
     */
    int getDuration() const;

    /*!
     * \brief isValid Metoda, která vypovídá o validitě obsažených dat.
     * \return True, pokud objekt obsahuje validní vzorky a vzorkovací frekcenci, jinak false.
     */
    bool isValid() const;

private:
    QVector<T> m_pcm;   //!< Vzorky akustického signálu.
    int m_sampleRate;   //!< Vzorkovací frekvence akustického signálu.

    /*!
     * \brief timeToSampleIndex Metoda vypočítá index vzorku v čase time, dle počtu vzorků
     *                          signálu a vzorkovací frekvenci.
     * \param time Čas v milisekundách.
     * \return Index, který odpovídá zadanému času. Pozor! Tento index nemusí být platný
     *         ve vektoru vzorků akustického signálu m_pcm.
     */
    int timeToSampleIndex(int time);
};

template<class T>
PcmSignal<T>::PcmSignal(const QVector<T> &pcm, int sampleRate) :
    m_pcm(pcm),
    m_sampleRate(sampleRate)
{ }

template<class T>
PcmSignal<T> PcmSignal<T>::invalid() {
    return PcmSignal<T>(QVector<T>(), 0);
}

template<class T>
QVector<T> PcmSignal<T>::getPcm() const {
    if (!isValid())
        return QVector<T>();

    return m_pcm;
}

template<class T>
int PcmSignal<T>::getDuration() const {
    if (!isValid())
        return 0;

    /* 1000 because result is in milliseconds */
    return (m_pcm.size() * 1000) / m_sampleRate;
}

template<class T>
bool PcmSignal<T>::isValid() const {
    return m_pcm.size() > 0 && m_sampleRate > 0;
}

template<class T>
int PcmSignal<T>::getSampleRate() const {
    if (!isValid())
        return 0;

    return m_sampleRate;
}

template<class T>
int PcmSignal<T>::timeToSampleIndex(int time) {
    if (!isValid())
        return 0;

    return (time * m_pcm.size()) / getDuration();
}

#endif
