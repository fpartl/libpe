#ifndef RAWLOADER_H
#define RAWLOADER_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QFile>
#include <QDataStream>
#include <QDebug>

#include "../pcmsignal.h"

/*!
 * \brief Třída PcmReader
 *
 * Třída představuje čtečku signálu ve formátu raw, tj. pouze surová data bez hlaviček a kódování.
 */
template <class T>
class RawLoader {

public:
    /*!
     * \brief read Metoda, která přečte zadaný soubor a vrátí vektor PCM vzorků.
     * \param fileName Cesta ke vstupnímu souboru.
     * \param sampleRate Protože raw soubor neobsahuje informaci o vzorkovací frekvenci, je nutné tuto
     *                   hodnotu udat ručně.
     * \return Objekt typu PCMSignal, který obsahuje vzorky PCM a informaci a vzorkovací frekvenci.
     */
    static PcmSignal<T> load(const QString& fileName, int sampleRate);
};

template<class T>
PcmSignal<T> RawLoader<T>::load(const QString &fileName, int sampleRate) {
    QVector<T> pcmSamples;

    QFile rawFile(fileName);
    if (!rawFile.open(QIODevice::ReadOnly)) {
        qDebug() << "readPcmFile: Nemohu číst soubor " << fileName;
        return PcmSignal<T>::invalid();
    }

    QDataStream in(&rawFile);
    T temp;
    while (!in.atEnd()) {
        if (in.readRawData(reinterpret_cast<char *>(&temp), sizeof(T)) != sizeof(T)) {
            qDebug() << "readPcmFile: Chyba při čtení souboru " << fileName;
            return PcmSignal<T>::invalid();
        }

        pcmSamples.append(temp);
    }

    return PcmSignal<T>(pcmSamples, sampleRate);
}

#endif
