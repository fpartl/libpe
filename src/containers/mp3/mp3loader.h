#ifndef MP3LOADER_H
#define MP3LOADER_H

#include <QObject>
#include <QVector>
#include <QFile>
#include <QDataStream>
#include <QSharedPointer>

#include <libfp/console/console.h>

#include "../pcmsignal.h"
#include "lame.h"

/*!
 * \brief Třída MP3Decoder
 *
 * Třída představuje MP3 dekodér, který dle zadaného mp3 záznamu vytvoří odpovídající
 * sekvenci PCM vzorků.
 */
template <class T>
class Mp3Loader : public QObject {

public:
    /*!
     * \brief decode Metoda, která provede samotné dekódování daného MP3 souboru a vrátí vektor PCM vzorků.
     * \param fileName Cesta k dekódovanému souboru.
     * \return Objekt typu PcmAudio, který obsahuje vzorky PCM a informaci a vzorkovací frekvenci.
     */
    static PcmSignal<T> load(const QString& fileName);

private:
    /*!
     * \brief MP3_BUFFER_SIZE Velikost buffer pro dekódování MP3 souborů. Tuto hodnotu se nedoporučuje měnit.
     */
    static constexpr int MP3_BUFFER_SIZE = 4096;

    /*!
     * \brief PCM_BUFFER_SIZE Velikost buffer pro PCM data. Tuto hodnotu se nedoporučuje měnit.
     */
    static constexpr int PCM_BUFFER_SIZE = 65536;

    /*!
     * \brief SUPPORTED_CHANNEL_COUNT Maximální počet kanálů, které může mp3 soubor obsahovat, aby byla možná jeho konverze.
     */
    static constexpr int SUPPORTED_CHANNEL_COUNT = 1;

    /*!
     * \brief SUPPORTED_MIN_BITRATE Minimální podporovaný bitrate.
     */
    static constexpr int SUPPORTED_MIN_BITRATE = 190;
};

#endif


template<class T>
PcmSignal<T> Mp3Loader<T>::load(const QString &fileName) {
    unsigned char m_mp3Buffer[MP3_BUFFER_SIZE];     // Buffer pro dekódování MP3.
    T m_pcmBuffer[PCM_BUFFER_SIZE];                 // Buffer pro dekódovaná data levého kanálu.
    T m_pcmBuffer2[PCM_BUFFER_SIZE];                // Buffer pro dekódovaná data pravého kanálu.

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        Console::writeError("Unable to open mp3 file.");
        return PcmSignal<T>::invalid();
    }

    QDataStream mp3Stream(&file);
    QSharedPointer<hip_global_flags> lame(hip_decode_init(), &hip_decode_exit);
    mp3data_struct header;

    QVector<T> pcm;
    int readed, decoded;
    do {
        readed = mp3Stream.readRawData(reinterpret_cast<char *>(m_mp3Buffer), MP3_BUFFER_SIZE);

        decoded = hip_decode_headers(lame.data(), m_mp3Buffer, static_cast<size_t>(readed), m_pcmBuffer, m_pcmBuffer2, &header);

        if (decoded > 0)
            std::copy(m_pcmBuffer, m_pcmBuffer + decoded, std::back_inserter(pcm));
    } while (readed != 0);

    if (header.header_parsed != 1 || header.stereo != SUPPORTED_CHANNEL_COUNT /*|| header.bitrate < SUPPORTED_MIN_BITRATE*/) {
        Console::writeError("Unable to parse mp3 source file.");

        if (header.header_parsed == 1 && header.stereo != SUPPORTED_CHANNEL_COUNT)
            Console::writeError(QString("Count of channels: %1").arg(header.stereo));

        if (header.header_parsed == 1 && header.bitrate < SUPPORTED_MIN_BITRATE)
            Console::writeError(QString("Bitrate is less than minimum (%1): %2").arg(SUPPORTED_MIN_BITRATE).arg(header.bitrate));

        return PcmSignal<T>::invalid();
    }

    PcmSignal<T> record(pcm, header.samplerate);

    file.close();
    return record;
}
