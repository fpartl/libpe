QT -= gui

TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++17
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    frequencyanalysis/dct.cpp \
    frequencyanalysis/fft.cpp \
    frequencyanalysis/kiss_fft/kiss_fft.c \
    frequencyanalysis/kiss_fft/kiss_fftr.c \
    mfcc/containers/mfccfile.cpp \
    mfcc/melfilterbank.cpp \
    mfcc/mfcc.cpp
#    mfccreconstruction/audiocomposer.cpp \
#    mfccreconstruction/audioscaler.cpp \
#    mfccreconstruction/espdrecover.cpp \
#    mfccreconstruction/segmentrecover.cpp \

HEADERS += \
    clustering/kmeans.h \
    containers/matrix.h \
    containers/mp3/mp3loader.h \
    containers/raw/rawloader.h \
    containers/pcmsignal.h \
    frequencyanalysis/dct.h \
    frequencyanalysis/fft.h \
    frequencyanalysis/kiss_fft/_kiss_fft_guts.h \
    frequencyanalysis/kiss_fft/kiss_fft.h \
    frequencyanalysis/kiss_fft/kiss_fftr.h \
    matching/dtw.h \
    math/normalization.h \
    math/statistics.h \
    metrics/metrics.h \
    mfcc/containers/mfccfile.h \
    mfcc/melfilterbank.h \
    mfcc/mfcc.h \
#    mfccreconstruction/audiocomposer.h \
#    mfccreconstruction/audioscaler.h \
#    mfccreconstruction/espdrecover.h \
#    mfccreconstruction/segmentrecover.h \
    plotting/vectorprinter.h \
    preprocess/preprocess.h \
    quantization/nonuniformquantization.h \
    quantization/quantization.h \
    quantization/uniformquantization.h \
    segmentation/iosignalsegmenter.h \
    segmentation/signalsegmenter.h \
    voicing/nvad.h \
    voicing/voicemodel.h \
    voicing/voicing.h \
    windowfunctions/hammingwindow.h \
    windowfunctions/windowfunction.h

############################################################################
### Přidání binárních souborů užitých knihoven a hlavičkových souborů ######
############################################################################
# Podmíněný překlad podle cílového ABI.
defineTest(containsABI) {
    res = $$find($$1, $$2)
    !count(res, 0): return(true)
    return(false)
}

# Podmíněný překlad podle cílového ABI.
defineReplace(getABIFromKit) {
    containsABI(QMAKESPEC, android_armv7): return (armeabi-v7a)
    containsABI(QMAKESPEC, android_x86): return (x86)
    return (linux_x64)
}

# Podmíněný překlad podle cílového ABI.
ANDROID_ABI = $$getABIFromKit()

equals(ANDROID_ABI, "") {
    error("Bohuzel je povoleno pouze sestaveni pro platformu Android (ABI armeabi-v7a a x86) a Linux x64.")
}

# Adresář, kde se očekávají binární soubory užitých knihoven. Pro jejich sestavení
# je třeba správně nakonfigurovat a spustit skript deps/buildDeps.sh.
LIBS_BUILD_DIR = $$PWD/../deps


# Adresář se sestavenou knihovnou libfp.
LIBFP_BUILD_DIR = $$LIBS_BUILD_DIR/libfp

# Načtení umístění hlavičkových souborů knihoven.
INCLUDEPATH += \
    $$LIBFP_BUILD_DIR/include


# Načtení umístění binárních souborů knihoven.
LIBS += \
    $$LIBFP_BUILD_DIR/build/android-$$ANDROID_ABI/liblibfp.a
