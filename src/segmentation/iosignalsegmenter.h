#ifndef IOSIGNALSEGMENTER_H
#define IOSIGNALSEGMENTER_H

#include <QObject>
#include <QIODevice>
#include <QVector>

#include "signalsegmenter.h"

/*!
 * \brief Třída IOSignalSegmenterSignals
 *
 * Tato třída obsahuje signály třídy IOSignalSegmenter. Je to takto odděleno z toho důvodu, že
 * Qt neumí šablonovat třídu se signály.
 */
class IOSignalSegmenterSignals : public QIODevice {
    Q_OBJECT

public:
    /*!
     * \brief IOSignalSegmenterSignals Konstruktor třídy.
     * \param parent Ukazatel na rodiče objektu kvůli automatickému uvolnění při dynamické alokaci.
     */
    explicit IOSignalSegmenterSignals(QObject *parent = nullptr) : QIODevice(parent) { }

signals:
    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<int> segment);

    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<unsigned int> segment);

    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<float> segment);

    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<double> segment);

    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<short> segment);

    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<unsigned short> segment);

    /*!
     * \brief segmentReady Signál, který nese nově sestavený segment zachyceného signálu.
     * \param segment Zachycený segment signálu.
     */
    void segmentReady(QVector<char> segment);


    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<int> segment);

    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<unsigned int> segment);

    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<float> segment);

    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<double> segment);

    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<short> segment);

    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<unsigned short> segment);

    /*!
     * \brief dataReceived Signál, který nese právě přijatá data.
     * \param segment Vektor právě přijatých dat.
     */
    void dataReceived(QVector<char> segment);
};


/*!
 * \brief Třída AudioWindower
 *
 * Třída reprezentuje vstupní zařízení, do kterého putuje zachytávaný signál (např. akustický signál ze zvukové karty). Pomocí
 * třídy SignalSegmenter rozkládá signál na segmenty o dané velikosti a překryvu. Připravené segmenty signálu jsou emitovány pomocí
 * signálů třídy IOSignalSegmenterSignals. Pozn. v Qt nelze udělat šablonovanou třídu s makrem Q_OBJECT, takže signály jsou různě
 * přetíženy a drženy v externí třídě -- možná trochu nelogicky, ale je to snad nejlepší cesta k úspěchu.
 */
template <class T>
class IOSignalSegmenter : public IOSignalSegmenterSignals {

public:
    /*!
     * \brief AudioWindower Konstruktor třídy.
     * \param segmentSize Požadovaná velikost vytvářených segmentů signálu.
     * \param overlap Hodnota překryvu jednotlivých segmentů signálu.
     * \param emitReceivedData Udává, zda se má emitovat signál s právě přijatými daty. Jedná se o volitelný parametr, který
     *                         je ve výchozím stavu nastavený na hodnotu false.
     * \param parent Ukazatel na rodiče objektu (kvůli dynamickému uvolňování).
     */
    explicit IOSignalSegmenter(int segmentSize, int overlap, bool emitReceivedData = false, QObject *parent = nullptr);

    /*!
     * \brief clear Vyprázdní buffer.
     */
    void clear();

    /*!
     * \brief segmentSize Vrátí velikost extrahovaných segmentů.
     * \return Velikost extrahovaných segmentů.
     */
    int segmentSize() const;

    /*!
     * \brief overlap Vrátí hodnotu překryvu sousedních segmentů.
     * \return Velikost překryvu sousedních segmentů.
     */
    int overlap() const;

private:
    SignalSegmenter<T> m_segmenter;     //!< Třída, která pomocí kruhového bufferu segmentuje přijatý signál.
    bool m_emitReceivedData;            //!< Příznak, který říká, zda se budou emitovat právě přijatá data.

    /*!
     * \brief emitRawData Pouze vytvoří vektor, do kterého překopíruje přijatá data a emituje signál dataBuffered. Spouštění této metody
     *                    je řízeno proměnnou m_emitReceivedData nastavené konstruktorem třídy.
     * \param data Ukazatel na syrová příchozí data ze zvukové karty.
     * \param maxSize Velikost příchozích dat v Bytech.
     */
    void emitRawData(const char *data, qint64 maxSize);

    /*!
     * \brief emitNewSegments Tato metoda postupně extrahuje všechny dostupné segmenty v cyklickém bufferu. Je volána automaticky při
     *                        každém přijetí nového signálu.
     */
    void emitNewSegments();

protected:
    /*!
     * \brief readData Slot je implementací virtuální metody z QIODevice. Nevyužívá se.
     */
    qint64 readData(char *data, qint64 maxSize) override;

    /*!
     * \brief writeData Slot, skrze kterého se předávají data ze zvukové karty.
                        Data se zde kopírují do bufferu. Poté se pomocí metody isWindowBuffered zkontroluje, jestli buffer obsahuje
                        okno, případně se volá metoda finalizeWindow. V případě, že je atribut extractTimeData nastaven na true, volá
                        se metoda emitRawTimeData.
     * \param data Ukazatel na data přijatá ze zvukové karty.
     * \param maxSize Velikost dat ze zvukové karty v Bytech.
     * \return Funkce vrací hodnotu parametru maxSize.
     */
    qint64 writeData(const char *data, qint64 maxSize) override;
};

template<class T>
IOSignalSegmenter<T>::IOSignalSegmenter(int segmentSize, int overlap, bool emitReceivedData, QObject *parent)
    : IOSignalSegmenterSignals(parent),
      m_segmenter(segmentSize, overlap)
{
    m_emitReceivedData = emitReceivedData;
}

template<class T>
void IOSignalSegmenter<T>::clear() {
    m_segmenter.clearBuffer();
}

template<class T>
int IOSignalSegmenter<T>::segmentSize() const {
    return m_segmenter.segmentSize();
}

template<class T>
int IOSignalSegmenter<T>::overlap() const {
    return m_segmenter.overlap();
}

template<class T>
void IOSignalSegmenter<T>::emitRawData(const char *data, qint64 maxSize) {
    int dataCount = maxSize / sizeof(T);

    QVector<T> copy;
    copy.reserve(dataCount);
    std::copy(reinterpret_cast<const T*>(data), reinterpret_cast<const T*>(data) + dataCount, std::back_inserter(copy));

    emit IOSignalSegmenterSignals::dataReceived(copy);
}

template<class T>
void IOSignalSegmenter<T>::emitNewSegments() {
    while (m_segmenter.hasNextSegment()) {
        emit IOSignalSegmenterSignals::segmentReady(m_segmenter.nextSegment());
    }
}

template<class T>
qint64 IOSignalSegmenter<T>::readData(char *data, qint64 maxSize) {
    Q_UNUSED(data)
    Q_UNUSED(maxSize)
    return -1;
}

template<class T>
qint64 IOSignalSegmenter<T>::writeData(const char *data, qint64 maxSize) {
    if (m_emitReceivedData)
        emitRawData(data, maxSize);

    m_segmenter.writeSignal(reinterpret_cast<const T *>(data), maxSize / sizeof(T));

    emitNewSegments();

    return maxSize;
}

#endif
