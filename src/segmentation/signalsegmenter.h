#ifndef SIGNALSEGMENTER_H
#define SIGNALSEGMENTER_H

#include <QObject>
#include <QVector>

#include <libfp/structures/cyclicbuffer.h>

/*!
 * \brief Třída AudioSegmenter
 *
 * Třída pomocí kruhového bufferu knihovny libfp umožňuje vstupní signál rozkládat na segmenty o dané velikosti
 * a překryvu. Třída je šablonována, takže můžeme libovolně volit datový typ vašich vzorků signálu.
 */
template <class T>
class SignalSegmenter {
public:
    /*!
     * \brief AudioSegmenter Konstruktor třídy.
     * \param segmentSize Velikost segmentů.
     * \param overlap Překryv segmentů.
     */
    explicit SignalSegmenter(int segmentSize, int overlap);

    /*!
     * \brief writeAudio Metoda, která zapíše vstupní data do bufferu.
     * \param data Reference na vektor vstupních dat.
     */
    void writeSignal(const QVector<T>& data);

    /*!
     * \brief writeSignal Přetížená metoda. Zapíše data do interního kruhového bufferu pomocí ukazatele a délky dat.
     * \param data Ukazatel na zapisovaná data.
     * \param size Počet zapisovaných prvků.
     */
    void writeSignal(const T* data, int size);

    /*!
     * \brief hasNextSegment Metoda zjistí, zda je v bufferu připraven alespoň jeden CELÝ segment. Zda je v bufferu alespoň
     *                       část segmentu, řekne metoda AudioSegmenter::isEmpty().
     * \return True, pokud je v bufferu připraven CELÝ segment, jinak false.
     */
    bool hasNextSegment();

    /*!
     * \brief size Metoda zjistí počet vzorků akustického signálu dostupných v bufferu.
     * \return Počet vzorků dostupných v bufferu.
     */
    int size();

    /*!
     * \brief nextSegment Metoda, která z bufferu extrahuje jeden segment. Pokud v bufferu není celý segment, extrahuje dostupná data
     *                    a doloží je nulami na požadovanou velikost segmentu. V případě prázdného bufferu vrátí prázdný vektor.
     * \return Vektor obsahují vzorky jednoho extrahovaného segmentu.
     */
    QVector<T> nextSegment();

    /*!
     * \brief isEmpty Metoda zjistí, zda jdou v bufferu dostupná nějaká data. Nemusí to být celý segment.
     * \return True, pokud buffer obsahuje nějaká data (i třeba neúplný segment), jinak false.
     */
    bool isEmpty();

    /*!
     * \brief clearBuffer Metoda vyprázdní interní cyklický buffer.
     */
    void clearBuffer();

    /*!
     * \brief segmentSize Vrátí velikost extrahovaných segmentů.
     * \return Velikost extrahovaných segmentů.
     */
    int segmentSize() const;

    /*!
     * \brief overlap Vrátí hodnotu překryvu sousedních segmentů.
     * \return Velikost překryvu sousedních segmentů.
     */
    int overlap() const;

private:
    CyclicBuffer<T> m_buffer;       //!< Vnitřní cyklický buffer.
    int m_segmentSize;              //!< Velikost extrahovaných segmentů signálu.
    int m_overlap;                  //!< Velikost překryvu jednotlivých segmentů.
};

template<class T>
SignalSegmenter<T>::SignalSegmenter(int segmentSize, int overlap) {
    m_segmentSize = segmentSize;
    m_overlap = overlap;
}

template<class T>
void SignalSegmenter<T>::writeSignal(const QVector<T> &data) {
    m_buffer.write(data);
}

template<class T>
void SignalSegmenter<T>::writeSignal(const T *data, int size) {
    m_buffer.write(data, size);
}

template<class T>
bool SignalSegmenter<T>::hasNextSegment() {
    return m_buffer.isBuffered(m_segmentSize);
}

template<class T>
int SignalSegmenter<T>::size() {
    return m_buffer.count();
}

template<class T>
QVector<T> SignalSegmenter<T>::nextSegment() {
    QVector<T> extracted = m_buffer.take(m_segmentSize, m_overlap);

    if (extracted.size() < m_segmentSize) {
        while (extracted.size() < m_segmentSize)
            extracted.append(0);
    }

    return extracted;
}

template<class T>
bool SignalSegmenter<T>::isEmpty() {
    return m_buffer.isEmpty();
}

template<class T>
void SignalSegmenter<T>::clearBuffer() {
    m_buffer.clear();
}

template<class T>
int SignalSegmenter<T>::segmentSize() const {
    return m_segmentSize;
}

template<class T>
int SignalSegmenter<T>::overlap() const {
    return m_overlap;
}

#endif
