#ifndef METRICS_H
#define METRICS_H

#include <type_traits>
#include <functional>
#include <numeric>
#include <cmath>

#include <QVector>

#include "../matching/dtw.h"

/*!
 * \brief Třída Metrics
 *
 * Třída obsahuje různé možnosti měření vzdáleností mezi body v příznakovém prostoru.
 */
class Metrics {
public:
    /*!
     * \brief euclDist Metoda vypočítá Euklidovskou vzdálenost (L2 normu) dvou bodů na přímce.
     * \param x Poloha prvního bodu.
     * \param y Poloha druhého bodu.
     * \return Vzdálenost mezi body.
     */
    template <class T>
    static T euclDist(const T& x, const T& y);

    /*!
     * \brief euclDist Metoda vypočítá Euklidovskou vzdálenost (L2 normu) dvou bodů v libovolném prostoru.
     * \param v1 Radius vektor, který určuje polohu prvního bodu.
     * \param v2 Radius vektor, který určuje polohu druhého bodu.
     * \return Vzdálenost mezi dvěma body.
     */
    template<class T>
    static T euclDistN(const QVector<T>& v1, const QVector<T>& v2);

    /*!
     * \brief chebyshev Chebyševova vzdálenost je definována jako maximum z absolutních hodnot rozdílů
     *                  jednotlivých složek.
     * \param v1 Radius vektor, který určuje polohu prvního bodu.
     * \param v2 Radius vektor, který určuje polohu druhého bodu.
     * \return Vzdálenost mezi dvěma body.
     */
    template <class T>
    static T chebyshev(const QVector<T>& v1, const QVector<T>& v2);

    /*!
     * \brief hamming Hammingova (Manhattanská) vzdálenost je definována jako suma všech absolutních hodnot rozdílů
     *                jednotlivých složek.
     * \param v1 Radius vektor, který určuje polohu prvního bodu.
     * \param v2 Radius vektor, který určuje polohu druhého bodu.
     * \return Vzdálenost mezi dvěma body.
     */
    template <class T>
    static T hamming(const QVector<T>& v1, const QVector<T>& v2);

    /*!
     * \brief dtw Vzdálenost definovaná funkcí DTW, která je popsána v souboru matching/dtw.h. Pro určení vzdálenosti
     *            dvou bodů je využita prostá Euclidova metrika.
     * \param v1 Radius vektor, který určuje polohu prvního bodu.
     * \param v2 Radius vektor, který určuje polohu druhého bodu.
     * \return Vzdálenost mezi dvěma body.
     */
    template <class T>
    static T dtw(const QVector<T>& v1, const QVector<T>& v2);
};

template<class T>
T Metrics::euclDist(const T &x, const T &y) {
    return static_cast<T>(std::abs(x - y));
}

template<class T>
T Metrics::euclDistN(const QVector<T> &v1, const QVector<T> &v2) {
    if (v1.isEmpty() || v1.size() != v2.size())
        return -1;

    T squareSum = 0;
    for (auto v1i = v1.begin(), v2i = v2.begin(); v1i != v1.end(); ++v1i, ++v2i)
        squareSum += std::pow(*v1i - *v2i, 2);

    return std::sqrt(squareSum);
}

template<class T>
T Metrics::chebyshev(const QVector<T> &v1, const QVector<T> &v2) {
    T max = std::numeric_limits<T>::min();

    for (auto v1i = v1.begin(), v2i = v2.begin(); v1i != v1.end(); ++v1i, ++v2i) {
        T sub = std::abs(*v1i - *v2i);

        if (sub > max)
            max = sub;
    }

    return max;
}

template<class T>
T Metrics::hamming(const QVector<T> &v1, const QVector<T> &v2) {
    T sum = 0;

    for (auto v1i = v1.begin(), v2i = v2.begin(); v1i != v1.end(); ++v1i, ++v2i)
        sum += euclDist(*v1i, *v2i);

    return sum;
}

template<class T>
T Metrics::dtw(const QVector<T> &v1, const QVector<T> &v2) {
    DTW<T> dtw([](T v1i, T v2i) -> double {
        return static_cast<double>(euclDist(v1i, v2i));
    });

    return static_cast<T>(dtw.calcWarpCost(v1, v2));
}

#endif
