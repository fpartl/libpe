#ifndef WINDOWFUNCTION_H
#define WINDOWFUNCTION_H

#include <QObject>
#include <QVector>
#include <QtDebug>

/*!
 * \brief Třída WindowFunction
 *
 * Třída reprezentuje obecnou váhovací funkci pro normalizaci segmentů obecného signálu.
 */
template <class T>
class WindowFunction : public QObject {
public:
    /*!
     * \brief WindowFunction Konstruktor třídy.
     * \param windowSize Počet vzorků okna pro normalizaci segmentu signálu.
     * \param parent Ukazatel na rodiče (kvůli automatické destrukci objektu).
     */
    explicit WindowFunction(int windowSize, QObject *parent = nullptr);

    /*!
     * \brief ~AudioNormalizer Destruktor třídy.
     */
    virtual ~WindowFunction() = default;

    /*!
     * \brief normalize Metoda provede normalizaci vstupního segmetu, tj. vynásobí všechny prvky daného
     *                  vektoru prvky váhovacího okna. Výsledek této operace uloží do nového vektoru, který
     *                  vrátí. V případě, že počet vzorků váhovacího okna a vstupního signálu je různý, metoda
     *                  vrátí prázdný vektor.
     * \param segment Vektor, který obsahuje vzorky segmentu pro normalizaci.
     * \return Normalizovaný segment.
     */
    QVector<float> normalize(const QVector<T> &segment);

    /*!
     * \brief denormalize Metoda provede denormalizaci segmentu, tj. vydělí všechny prvky daného vektoru
     *                    prvky váhovacího okna. Výsledek operace uloží do nového vektoru, který vrátí.
     *                    V případě, že počet vzorků váhovacího okna a vstupního singálu je různý, metoda
     *                    vrátí prázdný vektor.
     * \param segment Vektor, který obsahuje vzorky segmentu pro denormalizaci.
     * \return Denormalizovaný vektor.
     */
    QVector<float> denormalize(const QVector<float> &segment);

protected:
    /*!
     * \brief initWindow Virtuální metoda, jejíž úlohou je vypočítat vzorky daného normalizačního okna
     *                   (Hamming, Blackman, Triangular). Účelem funkce není alokace potřebného místa,
     *                   ale pouhý výpočet prvků vektoru m_window.
     */
    virtual void initWindow() = 0;

    /*!
     * \brief window Vektor, který obsahuje vzorky váhovacího okna.
     */
    QVector<float> m_window;
};

template<class T>
WindowFunction<T>::WindowFunction(int windowSize, QObject *parent) : QObject(parent) {
    if (windowSize <= 0)
        return;

    m_window.resize(windowSize);
}

template<class T>
QVector<float> WindowFunction<T>::normalize(const QVector<T> &segment) {
    if (segment.size() != m_window.size())
        return QVector<float>();

    QVector<float> normalized(segment.size());
    for (int i = 0; i < normalized.size(); i++) {
        normalized[i] = static_cast<float>(segment.at(i)) * m_window.at(i);
    }

    return normalized;
}

template<class T>
QVector<float> WindowFunction<T>::denormalize(const QVector<float> &segment) {
    if (segment.size() != m_window.size())
        return QVector<float>();

    QVector<float> denormalized(segment.size());
    for (int i = 0; i < segment.size(); i++)
        denormalized[i] = static_cast<float>(segment[i]) / m_window[i];

    return denormalized;
}

#endif
