#ifndef HAMMINGWINDOW_H
#define HAMMINGWINDOW_H

#include <QObject>
#include <QtMath>

#include "windowfunction.h"

/*!
 * \brief Třída HammingWindow
 *
 * Třída je konkrétní implementací virtuální třídy WindowFunction. Třída reprezentuje
 * Hammingovu váhovou funkci.
 */
template <class T>
class HammingWindow : public WindowFunction<T> {
public:
    /*!
     * \brief HammingWindow Konstruktor třídy.
     * \param windowSize Velikost vstupního segmentu.
     * \param parent Ukazatel na rodiče objektu (kvůli dynamickému uvolňování).
     */
    explicit HammingWindow(int windowSize, QObject *parent = nullptr);

private:
    static constexpr float alpha = 0.53836f;    //!< Konstanta se využívá při výpočtu Hammingova okna.
    static constexpr float beta = 0.46164f;     //!< Konstanta se využívá při výpočtu Hammingova okna.

    /*!
     * \brief initWindow Konkrétní implementace virtuální metody initWindow. Pomocí
     *                   koeficientů ALPHA a BETA počítá průběh Hammingovy funkce.
     */
    void initWindow() override;
};

template <class T>
HammingWindow<T>::HammingWindow(int windowSize, QObject *parent) : WindowFunction<T>(windowSize, parent) {
    initWindow();
}

template <class T>
void HammingWindow<T>::initWindow() {
    if (WindowFunction<T>::m_window.size() <= 1) // kvůli dělení nulou
        return;

    for (int n = 0; n < WindowFunction<T>::m_window.size(); n++) {
        WindowFunction<T>::m_window[n] = HammingWindow::alpha - HammingWindow::beta
                                                * qCos((2 * M_PI * n) / (WindowFunction<T>::m_window.size() - 1));
    }
}

#endif
