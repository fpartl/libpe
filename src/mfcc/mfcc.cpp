#include "mfcc.h"

MFCC::MFCC(int sampleRate, int filtersCount, int espdSize)
        : m_filters(espdSize, filtersCount, sampleRate)
{ }

QVector<float> MFCC::calculate(QVector<float> espd, int count) {
    if (espd.size() != m_filters.filterSize())
        return QVector<float>();

    QVector<float> mels = calcMelCoefs(espd);
    logaritmize(mels);

    return calcKepsCoefs(mels, count);
}

QVector<float> MFCC::calcDelta(const QVector<float> &mfcc_k, const QVector<float> &mfcc_k1) {
    return mfcc_k - mfcc_k1;
}

QVector<float> MFCC::calcDeltaDelta(const QVector<float> &delta_k, const QVector<float> &delta_k1) {
    return delta_k - delta_k1;
}

QVector<float> MFCC::calcMelCoefs(QVector<float> espd) {
    QVector<float> mels(m_filters.count());

    for (int i = 0; i < mels.size(); ++i) {
        mels[i] = 0.0f;

        for (int j = 0; j < m_filters.getFilter(i).size(); ++j)
            mels[i] += static_cast<float>(qFabs(static_cast<double>(espd.at(j) * m_filters.getFilter(i).at(j))));
    }

    return mels;
}

void MFCC::logaritmize(QVector<float> &melCoefs) {
    for (float& c : melCoefs) {
        if (c > 0.0f)
            c = static_cast<float>(qLn(c));
    }
}

QVector<float> MFCC::calcKepsCoefs(QVector<float> melCoefs, int count) {
    if (count <= 0 || count > m_filters.count())
        count = m_filters.count();

    QVector<float> keps(count);
    for (int i = 0; i < keps.size(); ++i) {
        keps[i] = DCT::dct(melCoefs, i);
        keps[i] *= getNormFactor(i);
    }

    return keps;
}

float MFCC::getNormFactor(int i) {
    return (i == 0)
            ? static_cast<float>(qSqrt(1.0 / static_cast<double>(m_filters.count())))
            : static_cast<float>(qSqrt(2.0 / static_cast<double>(m_filters.count())));
}

QVector<float> operator-(const QVector<float> &v1, const QVector<float> &v2) {
    if (v1.size() != v2.size())
        return QVector<float>();

    QVector<float> result;
    result.reserve(v1.size());

    std::transform(v1.begin(), v1.end(), v2.begin(), std::back_inserter(result), std::minus<float>());

    return result;
}
