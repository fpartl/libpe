#ifndef MFCC_H
#define MFCC_H

#include <QObject>
#include <QVector>
#include <QMutableVectorIterator>

#include "melfilterbank.h"
#include "../frequencyanalysis/dct.h"

/*!
 * \brief operator- Přetížení binárního operátoru odečítání. Využívá se při výpočtu delta
 *                  a delta-delta (akceleračních) koeficientů.
 * \param v1 První vektor (menšenec), od kterého bude odečítán druhý vektor.
 * \param v2 Druhý vektor (menšitel), o jehož hodnotu je snižována hodnota prvního vektoru.
 * \return Rozdíl dvou vektorů.
 */
QVector<float> operator-(const QVector<float>& v1, const QVector<float>& v2);

/*!
 * \brief Třída MFCC
 *
 * Třída obsahuje metody pro parametrizaci vstupních vektorů odhadů výkonových spekter. Využívá
 * třídy MelFilterBank, která obsahuje patřičné trojúhelníkové filtry. Kvůli konstrukci těchto filtrů
 * je třeba velikosti vstupních vektorů definovat okamžitě během konstrukce objektu. Při přijetí
 * vektorů jiné velikosti se pak emituje signál error.
 */
class MFCC {
public:
    /*!
     * \brief MFCC Konstruktor třídy.
     * \param sampleRate Frekvence vzorkování parametrizovaného akustického signálu.
     * \param filtersCount Počet filtrů využitých při parametrizaci. Tato hodnota odpovídá počtu
     *                     počítaných MFC koeficientů.
     * \param espdSize Počet prvků vstupních vektorů odhadů výkonových spekter.
     */
    explicit MFCC(int sampleRate, int filtersCount, int espdSize);

    /*!
     * \brief calculate Metoda vypočítá samotné MFC koeficienty podle zadaného vektoru odhadu výkonového
     *                  spektra. Pokud se počet přijatých prvků nerovná hodnotě m_espdSize, je vrácen prázdný vektor.
     *                  V případě neplatného počtu kepstrálních koeficientů je vypočítáno všech m_filtersCount koeficientů.
     * \param espd Vstupní vektor odhadu výkonového spektra.
     * \param count Počet požadovaných kepstrálních koeficientů.
     * \return Vektor vypočítaných MFC koeficientů. Velikost tohoto vektoru odpovídá hodnotě m_filtersCount.
     */
    QVector<float> calculate(QVector<float> espd, int count);

    /*!
     * \brief calcDelta Tato metoda pomocí zadaného c_k a c_{k-1} vektoru MFCC koeficientů vypočítá jejich
     *                  derivaci delta_k.
     * \param mfcc_k Vektor MFCC koeficientů c_k.
     * \param mfcc_k1 Vektor MFCC koeficientů c_{k-1}.
     * \return Výsledek derivace -- delta koeficienty.
     */
    QVector<float> calcDelta(const QVector<float>& mfcc_k, const QVector<float>& mfcc_k1);

    /*!
     * \brief calcDeltaDelta Tato metoda pomocí zadaného delta_k a delta_{k-1} vektoru diferencí MFCC koeficientů
     *                       vypočítá jejich derivaci acc_k.
     * \param delta_k Vektor diferencí MFCC koeficientů delta_k.
     * \param delta_k1 Vektor diferencí MFCC koeficientů delta_k1.
     * \return Výsledek druhé derivace -- delta-delta (akcelerační) koeficienty.
     */
    QVector<float> calcDeltaDelta(const QVector<float>& delta_k, const QVector<float>& delta_k1);

    /*!
     * \brief calcMelCoefs Tato metoda vypočítá melovské koeficienty vstupního odhadu výkonové spektrální
     *                     hustoty, tj. skalární součiny vektorů jednotlivých melovských filtrů a vstupního
     *                     vektoru. Počet výsledných melovských koeficientů je stejný jako počet melovských
     *                     filtrů.
     * \param espd Vstupní vektor odhadu výkonové spektrální hustoty.
     * \return Vektor melovských koeficientů.
     */
    QVector<float> calcMelCoefs(QVector<float> espd);

private:
    MelFilterBank m_filters;    //!< Objekt, který obsahuje samotné trojúhelníkové filtry.

    /*!
     * \brief logaritmize Metoda aplikuje funkci přirozeného logaritmu na všechny zadané melovské koeficienty.
     * \param melCoefs Melovské koeficienty, které budou logaritmovány.
     */
    void logaritmize(QVector<float> &melCoefs);

    /*!
     * \brief calcKepsCoefs Metoda podle zadaného vektoru melovských koeficientů vypočte odpovídající kepstrální
     *                      koeficienty užitím DCT.
     * \param melCoefs Vstupní melovské koeficienty.
     * \param count Počet požadovaných koeficientů. Pokud je toto číslo mimo rozsah melCoefs, bude vypočteno
     *              všech m_filters.count() koeficientů.
     * \return Výstupní kepstrální koeficienty.
     */
    QVector<float> calcKepsCoefs(QVector<float> melCoefs, int count);

    /*!
     * \brief getNormFactor Získá normalizační faktor dle jeho definice.
     * \param i Parametr pro získání normalizačního faktoru.
     * \return Normalizační faktor.
     */
    inline float getNormFactor(int i);
};

#endif
