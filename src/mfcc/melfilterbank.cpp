#include "melfilterbank.h"

MelFilterBank::MelFilterBank(int espdSize, int filterCount, int sampleRate) {
    // Děleno dvěma protože rozsah ESPD je 0 až Nyquistova frekvence.
    initFilters(filterCount, espdSize, sampleRate / 2);
}

const QVector<QVector<float>> &MelFilterBank::getFilters() const {
    return m_melFilters;
}

const QVector<float>& MelFilterBank::getFilter(int th) const {
    return m_melFilters[th];
}

float MelFilterBank::getFilterValue(int thFilter, int at) const {
    return m_melFilters[thFilter][at];
}

int MelFilterBank::filterSize() const {
    return m_melFilters.first().size();
}

int MelFilterBank::espdSize() const {
    return filterSize();
}

int MelFilterBank::count() const {
    return m_melFilters.size();
}

int MelFilterBank::size() const {
    return count();
}

void MelFilterBank::initFilters(int filterCount, int espdSize, int maxFrequency) {
    m_melFilters.reserve(filterCount);

    QVector<int> filterPoints = distributeFilters(filterCount, espdSize, maxFrequency);
    for (int m = 1; m < filterPoints.count() - 1; m++) {
        QVector<float> filter(espdSize);

        for (int k = 0; k < filter.size(); k++) {
            if (k < filterPoints[m - 1]) {
                filter[k] = 0; // nuluju všechno před trojúhelníkem
            }
            else if (filterPoints[m - 1] <= k && k <= filterPoints[m]) { // hrabu se do kopce
                if ((filterPoints[m] - filterPoints[m - 1]) != 0)
                    filter[k] = (static_cast<float>(k) - filterPoints[m - 1]) / (filterPoints[m] - filterPoints[m - 1]);
                else
                    filter[k] = 1;
            }
            else if (filterPoints[m] <= k && k <= filterPoints[m + 1]) { // jedu z kopce
                if ((filterPoints[m + 1] - filterPoints[m]) != 0)
                    filter[k] = (filterPoints[m + 1] - static_cast<float>(k)) / (filterPoints[m + 1] - filterPoints[m]);
                else
                    filter[k] = 1;
            }
            else filter[k] = 0; // nuluju všechno za trojúhelníkem
        }

        m_melFilters.append(filter);
    }
}

QVector<int> MelFilterBank::distributeFilters(int filterCount, int espdSize, int maxFrequency) {
    QVector<float> filterPoints(filterCount + 2); // + 2 protože mám ještě nejnižší frekvenci a nejvyšší frekvenci

    // Rovnoměrné rozmístění na melovské škále.
    filterPoints[0] = this->hzToMels(0.0f);
    filterPoints[filterPoints.size() - 1] = this->hzToMels(maxFrequency);

    float step = (filterPoints[filterPoints.size() - 1] - filterPoints[0]) / (filterCount + 1);

    for (int i = 1; i < filterPoints.size() - 1; i++)
        filterPoints[i] = filterPoints[i - 1] + step;

    // Škálování bodů na klasickou linerání škálu s rozlišením espdSize.
    double scaleFactor = static_cast<double>(espdSize) / static_cast<double>(maxFrequency);

    QVector<int> espdFilterBounds;
    espdFilterBounds.reserve(filterPoints.size());

    for (const float &p : filterPoints) {
        int scaled = qFloor(scaleFactor * static_cast<double>(melsToHz(p)));

        espdFilterBounds.append((scaled > espdSize - 1) ? espdSize - 1 : scaled);
    }

    return espdFilterBounds;
}

inline float MelFilterBank::hzToMels(float fHz) const {
    return 1125.0f * static_cast<float>(qLn(1. + (static_cast<double>(fHz) / 700.)));
}

inline float MelFilterBank::melsToHz(float fMel) const {
    return 700.0f * static_cast<float>(qExp(static_cast<double>(fMel) / 1125.) - 1.);
}
