#ifndef FILTERBANK_H
#define FILTERBANK_H

#include <QObject>
#include <QVector>
#include <QtMath>

/*!
 * \brief Třída MelFilterBank obsahuje metody pro generování a uchovávání banky melovských filtrů.
 */
class MelFilterBank {
public:
    /*!
     * \brief MelFilterBank Konstruktor třídy.
     * \param espdSize Počet prvků vektoru odhadu výkonové spektrální hustoty segmentu.
     * \param filterCount Požadovaný počet filtrů banky melovských filtrů.
     * \param sampleRate Frekvence vzorkování vstupního signálu, která bude následně dělením 2 převedena
     *                   na maximální frekvenci v signálu (Nyquistova frekvence).
     */
    explicit MelFilterBank(int espdSize, int filterCount, int sampleRate);

    /*!
     * \brief getFilters Metoda vrátí celou banku melovských filtrů. Vrácené hodnoty jsou určeny pouze
     *                   ke čtení.
     * \return Celá banka melovských filtrů.
     */
    const QVector<QVector<float>>& getFilters() const;

    /*!
     * \brief getFilter Metoda vrátí požadovaný filtr. V případě chyby metoda vrací prázdný vektor.
     * \param th Index požadovaného filtru.
     * \return Vektor vzorků požadovaného filtru.
     */
    const QVector<float>& getFilter(int th) const;

    /*!
     * \brief getValue Metoda pro získání konkrétního vzorku konkrétního filtru. Při zadání indexů mimo
     *                 rozsah funkce vrátí výchozí hodnotu, tj. nulu.
     * \param thFilter Index filtru, ze kterého je požadována hodnota (v rozsahu 0 až m_filterCount - 1).
     * \param at Index konkrétního vzorku filtru (v rozsahu 0 až m_espdSize - 1).
     * \return Hodnota daného filtru v daném bodě nebo hodnota UNDEFINED při chybě.
     */
    float getFilterValue(int thFilter, int at) const;

    /*!
     * \brief getFilterSum Metoda vrací hodnotu součtu všech vzorků zadaného filtru. V případě, že je zadána
     *                     hodnota mimo rozsah, metoda vrací výchozí hodnotu, tj. nulu.
     * \param thFilter Index filtru, ze kterého se požaduje součet.
     * \return Součet hodnot daného filtru.
     */
    float getFilterSum(int thFilter) const;

    /*!
     * \brief filterSize Metoda vrací počet prvků odhadu výkonové spektrální hustoty segmentu, tj. počet vzorků.
     * \return Počet prvků odhadu výkonové spektrální hustoty.
     */
    int filterSize() const;

    /*!
     * \brief espdSize Přetížená metoda, která má stejné chování jako filterSize.
     * \return Počet prvků odhadu výkonové spektrální hustoty.
     */
    int espdSize() const;

    /*!
     * \brief count Vrací počet filtrů banky melovských filtrů.
     * \return Počet filtrů banky melovských filtrů.
     */
    int count() const;

    /*!
     * \brief size Přetížená metoda, která má stejné chování jako metoda count.
     * \return Počet filtrů banky melovských fitrů.
     */
    int size() const;

private:
    QVector<QVector<float>> m_melFilters;       //!< Filtry banky melovských filtrů.

    /*!
     * \brief initFilters Metoda provede inicializaci banky melovských filtrů podle argumentů konstruktoru.
     * \param filterCount Počet generovaných filtrů melovské banky.
     * \param espdSize Počet vzorků každého filtru.
     * \param sampleRate Frekvence vzorkování vstupního signálu.
     */
    void initFilters(int filterCount, int espdSize, int maxFrequency);

    /*!
     * \brief distributeFilters Metoda vypočítá rovnoměrné rozložení filtrů v melspektru.
     * \param filterCount Počet filtrů melovské banky.
     * \param espdSize Počet prvků vstupního odhadu výkonové spektrální hustoty.
     * \param sampleRate Frekvence vzorkování vstupního signálu.
     * \return Vektor, který obsahuje body, z nichž se později vytvoří jednotlivé filtry.
     */
    QVector<int> distributeFilters(int filterCount, int espdSize, int maxFrequency);

    /*!
     * \brief hzToMels Metoda, která provádí převod jednotek Hz na jednotky Mel.
     * \param fHz Hodnota k převedení.
     * \return Hodnota fHz převedená do jednotek mel-spektra.
     */
    inline float hzToMels(float fHz) const;

    /*!
     * \brief melsToHz Metoda, která provádí převod jednotek Mel na jednotky Hz.
     * \param fMel Hodnota k převedení.
     * \return Hodnota fMel převedená do jednotek lineárního spektra.
     */
    inline float melsToHz(float fMel) const;
};

#endif
