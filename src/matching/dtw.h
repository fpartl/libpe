#ifndef DTW_H
#define DTW_H

#include <QVector>
#include <functional>

#include "../containers/matrix.h"

/*!
 * \brief Třída DTW
 *
 * Třída představuje implementace algoritmu Dynamic time warping (DTW), kterým lze měřit podobnost
 * dvou signálů, které nemusejí mít stejnou délku. V konstruktoru třídy je třeba uvést funkci, kterou
 * bude počítána vzdálenost mezi dvěma body (kvůli možnosti různých typů prvků).
 */
template <class T>
class DTW {
public:
    /*!
     * \brief DTW Konstruktor třídy. Dynamicky nealokuje žádný paměťový prostor.
     * \param metric Funkce (lambda, ukazatel), jejímž účelem je výpočet vzdálenosti mezi dvěma body.
     *               Prototyp funkce musí být double(T, T). Jiným zápisem [](T, T) -> double.
     */
    explicit DTW(std::function<double(T, T)> metric);

    /*!
     * \brief calcWarpCost Metoda spočítá hodnotu DTW pro dva zadané signály. Touto hodnotou je myšlena
     *                     nejvyšší hodnota v matici DTW, která se vyskytuje v pravém horním rohu. Pokud
     *                     alespoň jeden ze signálů neobsahuje žádné prvky, metoda vrací hodnotu +inf.
     * \param s1 První signál.
     * \param s2 Druhý signál.
     * \return Výsledek DTW, absolutní podobnost zadaných signálů, nebo +inf, pokud alespoň jeden ze signálů
     *         neobsahuje žádné prvky.
     */
    double calcWarpCost(const QVector<T>& s1, const QVector<T>& s2);

    /*!
     * \brief calcWarpMat Tato metoda vypočítá matici vzdáleností mezi jednotlivými vzorky signálu.
     *                    Algoritmus výpočtu jednotlivých vzorků této matice je k dispozici v popisu
     *                    DTW na https://en.wikipedia.org/wiki/Dynamic_time_warping#Implementation.
     * \param s1 První signál.
     * \param s2 Druhý signál.
     * \return Matice vzdáleností.
     */
    Matrix<double> calcWarpMat(const QVector<T>& s1, const QVector<T>& s2);

private:
    std::function<double(T, T)> m_metric;    //!< Funkce pro výpočet vzdálenosti dvou vzorků signálu.
};

template<class T>
DTW<T>::DTW(std::function<double(T, T)> metric)
    : m_metric(metric)
{ }

template<class T>
double DTW<T>::calcWarpCost(const QVector<T>& s1, const QVector<T>& s2) {
    if (s1.isEmpty() || s2.isEmpty())
        return std::numeric_limits<double>::infinity();

    Matrix<double> distances = calcWarpMat(s1, s2);

    return distances[distances.rows() - 1][distances.cols() - 1];
}

template<class T>
Matrix<double> DTW<T>::calcWarpMat(const QVector<T>& s1, const QVector<T>& s2) {
    int i, j;

    Matrix<double> dists(s1.size(), s2.size(), 0.0);

    // Nastavím první prvek matice.
    dists[0][0] = m_metric(s1[0], s2[0]);

    // Nastavím první sloupec a první řádek matice.
    for (i = 1, j = 0; i < dists.rows(); ++i)
        dists[i][j] = m_metric(s1[i], s2[j]) + dists[i - 1][j];

    for (i = 0, j = 1; j < dists.cols(); ++j)
        dists[i][j] = m_metric(s1[i], s2[j]) + dists[i][j - 1];

    // Zbylé prvky matice stanovím podle algoritmu DTW.
    for (i = 1; i < dists.rows(); ++i) {
        for (j = 1; j < dists.cols(); ++j) {
            dists[i][j] = m_metric(s1[i], s2[j]) + std::min({
                                                        dists[i - 1][j],
                                                        dists[i][j - 1],
                                                        dists[i - 1][j - 1]
                                                    });
        }
    }

    return dists;
}

#endif
