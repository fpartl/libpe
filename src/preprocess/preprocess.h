#ifndef PREPROCESS_H
#define PREPROCESS_H

#include <QVector>

#include "../math/statistics.h"

/*!
 * \brief Třída Preprocess
 *
 * Třída obsahuje skupinu statických metod pro předzpracování signálů.
 */
class Preprocess {
public:
    /*!
     * \brief removeDC Hlavní vlastností akustického signálu je, že má nulovou střední hodnotu.
     *                 Pro signály pořízené některými mikrofony toto nemusí platit. Tato metoda
     *                 tedy od každého vzorku zadaného signálu odečte střední hodnotu signálu,
     *                 čímž se vlastnost akustického signálu obnoví.
     * \param signal Signál, jehož střední hodnota bude posunuta k nule.
     */
    template <class T>
    static void removeDC(QVector<T>& signal);

    /*!
     * \brief removeDC2 Hlavní vlastností akustického signálu je, že má nulovou střední hodnotu.
     *                  Pro signály pořízené některými mikrofony toto nemusí platit. Tato metoda
     *                  tedy od každého vzorku zadaného signálu odečte střední hodnotu signálu,
     *                  čímž se vlastnost akustického signálu obnoví.
     * \param signal Signál, jehož střední hodnota bude posunuta k nule.
     */
    template <class T>
    static QVector<float> removeDC2(const QVector<T>& signal);

    /*!
     * \brief preemhasis Preemfáze je předzpracování signálu, kdy jsou horní frekvence posíleny
     *                   a základní hlasivková frekvence snížená, a tím i výraznost formantové struktury.
     *                   To vede k lepšímu rozpoznání mluvčího. Jedná se defacto o FIR filtr prvního
     *                   řádu, kde koeficient lambda (preempCoef) bývá obvykle nastaven na hodnotu
     *                   0.95 nebo 0.97.
     * \param segment Segment signálu, který projde preemfází.
     * \param preempCoef Koeficient preemfáze.
     * \return Fitrovaný segment signálu.
     */
    template <class T>
    static void preemphasis(QVector<T>& segment, float preempCoef);

    /*!
     * \brief preemhasis2 Preemfáze je předzpracování signálu, kdy jsou horní frekvence posíleny
     *                    a základní hlasivková frekvence snížená, a tím i výraznost formantové struktury.
     *                    To vede k lepšímu rozpoznání mluvčího. Jedná se defacto o FIR filtr prvního
     *                    řádu, kde koeficient lambda (preempCoef) bývá obvykle nastaven na hodnotu
     *                    0.95 nebo 0.97.
     * \param segment Segment signálu, který projde preemfází.
     * \param preempCoef Koeficient preemfáze.
     * \return Fitrovaný segment signálu.
     */
    template <class T>
    static QVector<float> preemphasis2(const QVector<T>& segment, float preempCoef);
};

template<class T>
void Preprocess::removeDC(QVector<T> &signal) {
    T mean = static_cast<T>(Statistics::arithmeticMean<T>(signal));

    // Průměr odečtu od jednotlivých složek signálu.
    std::transform(signal.begin(), signal.end(), signal.begin(),
        [mean](T i) -> T {
            return i - mean;
        }
    );
}

template<class T>
QVector<float> Preprocess::removeDC2(const QVector<T> &signal) {
    QVector<float> newSignal;
    newSignal.reserve(signal.size());

    float mean = static_cast<float>(Statistics::arithmeticMean<T>(signal));

    std::transform(signal.begin(), signal.end(), std::back_inserter(newSignal),
        [mean](T i) -> float {
            return static_cast<float>(i) - mean;
        }
    );

    return newSignal;
}

template<class T>
void Preprocess::preemphasis(QVector<T> &segment, float preempCoef) {
    if (segment.size() < 2)
        return;

    for (auto i = segment.begin() + 1; i != segment.end(); ++i)
        *i = static_cast<float>(*i)
                - (preempCoef * static_cast<float>(*(i - 1)));

    segment.first() = 0;
}

template<class T>
QVector<float> Preprocess::preemphasis2(const QVector<T> &segment, float preempCoef) {
    if (segment.size() < 2)
        return QVector<float>();

    QVector<float> preemSegment(segment.size());

    preemSegment[0] = static_cast<float>(segment.first());
    for (int i = 1; i < preemSegment.size(); ++i)
        preemSegment[i] = static_cast<float>(segment[i])
                            - (preempCoef * static_cast<float>(segment[i - 1]));

    return preemSegment;
}

#endif
