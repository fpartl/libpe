#ifndef DCT_H
#define DCT_H

#include <QVector>
#include <QtMath>

/*!
 * \brief Třída DCT
 *
 * Třída DCT obsahuje možnost výpočtu dopředné i zpětné kosinové transformace.
 */
class DCT {
public:
    /*!
     * \brief dct Metoda podle zadaného signálu vypočítá mth-tý koeficient dopředné
     *            kosinové transforamace.
     * \param xi Vstupní signál.
     * \param mth Požadovaný koeficient.
     * \return Hodnota mth-ého koeficientu dopředné kosinové transformace.
     */
    static float dct(QVector<float> xi, int mth);

    /*!
     * \brief idct Metoda podle zadaných koeficientů spočítá inverzi kosinové transformace
     *             a vrátí mth-tý koeficient.
     * \param xi Koeficienty diskrétní kosinové transformace.
     * \param mth Požadovaný koeficient.
     * \return Hodnota mth-ého koeficientu zpětné kosinové transformace.
     */
    static float idct(QVector<float> xi, int mth);
};

#endif
