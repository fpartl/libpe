﻿#include "fft.h"

FFT::FFT(int estVectorSize, QObject *parent) : QObject(parent) {
    if (!initMaxSegmentSize(estVectorSize))
        return;

    initEspdSize();

    if (!initKissFFT())
        return;
}

FFT::~FFT() {
    free(m_fftCfg);
    free(m_ifftCft);
}

int FFT::espdSize() const {
    return m_espdSize;
}

int FFT::maxInputSize() const {
    return m_maxInputSize;
}

QVector<float> FFT::computeEspd(const QVector<float> &input) {
    if (input.isEmpty() || input.size() > m_maxInputSize)
        return QVector<float>();

    QVector<float> espd(m_espdSize);
    computeEspd(input, espd);

    return espd;
}

bool FFT::computeEspd(const QVector<float> &input, QVector<float> &espd) {
    if (input.isEmpty() || input.size() > m_maxInputSize) {
        espd.clear();
        return false;
    }

    if (espd.size() != m_espdSize)
        espd.resize(m_espdSize);

    QVector<kiss_fft_cpx> cpx = realFFt(input);
    if (cpx.isEmpty()) {
        espd.clear();
        return false;
    }

    return euclidNorm(cpx, espd);
}

QVector<kiss_fft_cpx> FFT::realFFt(const QVector<float> &input) {
    if (input.isEmpty() || input.size() > m_maxInputSize)
        return QVector<kiss_fft_cpx>();

    QVector<kiss_fft_cpx> cpx(m_espdSize);
    realFFt(input, cpx);

    return cpx;
}

bool FFT::realFFt(const QVector<float> &input, QVector<kiss_fft_cpx> &cpx) {
    if (input.isEmpty() || input.size() > m_maxInputSize) {
        cpx.clear();
        return false;
    }

    QVector<float> fftInput = (input.size() < m_maxInputSize)
                                    ? supplementSegment(input)
                                    : input;

    if (cpx.size() != m_espdSize)
        cpx.resize(m_espdSize);

    kiss_fftr(m_fftCfg, fftInput.constData(), cpx.data());
    return true;
}

QVector<float> FFT::invRealFFT(const QVector<kiss_fft_cpx> &cpx) {
    if (cpx.size() != m_espdSize)
        return QVector<float>();

    QVector<float> output(m_maxInputSize);
    invRealFFT(cpx, output);

    return output;
}

bool FFT::invRealFFT(const QVector<kiss_fft_cpx> &cpx, QVector<float> &output) {
    if (cpx.size() != m_espdSize) {
        output.clear();
        return false;
    }

    if (output.size() != m_maxInputSize)
        output.resize(m_maxInputSize);

    kiss_fftri(m_ifftCft, cpx.constData(), output.data());

    // scale factor
    for (int i = 0; i < output.size(); i++)
        output[i] /= static_cast<float>(m_espdSize);

    return true;
}

bool FFT::initMaxSegmentSize(int estSegSize) {
    if (estSegSize <= 0)
        return false;

    for (m_maxInputSize = 1; m_maxInputSize < estSegSize; m_maxInputSize *= 2);

    return true;
}

void FFT::initEspdSize() {
    m_espdSize = (m_maxInputSize / 2) + 1;
}

bool FFT::initKissFFT() {
    m_fftCfg = kiss_fftr_alloc(m_maxInputSize, 0, nullptr, nullptr);
    m_ifftCft = kiss_fftr_alloc(m_maxInputSize, 1, nullptr, nullptr);

    return m_fftCfg && m_ifftCft;
}

QVector<float> FFT::supplementSegment(const QVector<float> &input) {
    QVector<float> fftInput(m_maxInputSize, 0.0f);
    std::copy(input.begin(), input.end(), fftInput.begin());

    return fftInput;
}

bool FFT::euclidNorm(const QVector<kiss_fft_cpx> &cpx, QVector<float> &espd) {
    if (cpx.isEmpty()) {
        espd.clear();
        return false;
    }

    if (cpx.size() != espd.size())
        espd.resize(cpx.size());

    QVectorIterator<kiss_fft_cpx> i(cpx);
    QMutableVectorIterator<float> j(espd);
    while (i.hasNext() && j.hasNext()) {
        kiss_fft_cpx c = i.next();
        j.next();

        float magnitude = static_cast<float>(qSqrt(static_cast<double>(c.r * c.r) + static_cast<double>(c.i * c.i)));
        j.setValue(magnitude);
    }

    if (i.hasNext()) {      // Tohle znamená problém, protože jsem zřejmě nedojel na konec vstupního vektoru.
        espd.clear();
        return false;
    }

    return true;
}
