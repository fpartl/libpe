#ifndef FFT_H
#define FFT_H

#include <QObject>
#include <QtMath>

#include <QVector>
#include <QVectorIterator>
#include <QMutableVectorIterator>

#include "kiss_fft/kiss_fft.h"
#include "kiss_fft/kiss_fftr.h"

/*!
 * \brief Třída FFT
 *
 * Tato třída reprezentuje dopřednou a zpětnou diskrétní Fourierovu transformaci. Je postavena na
 * knihovně KissFFT. Velikost vstupních vektorů je třeba stanovit už při konstrukci objektu. Je to
 * z důvodu dynamické alokace potřebných zdrojů. Při pozdějších vloženích větších vektorů metody třídy
 * vrací prázdné vektory a k žádnému výpočtu nedochází.
 */
class FFT : public QObject {
public:
    /*!
     * \brief FFT Konstruktor třídy. Provede výpočet maximální velikosti vstupních vektorů a podle
     *            toho i výpočet velikosti výsledných vektorů odhadu výkonové spektrální hustoty.
     * \param estVectorSize Předpokládaná velikost vstupních vektorů signálu.
     * \param parent Ukazatel na rodičovský objekt (kvůli dynamickému uvolnění).
     */
    explicit FFT(int estVectorSize, QObject *parent = nullptr);

    /*!
     * Destruktor třídy.
     */
    ~FFT();

    /*!
     * \brief spectrumSize Metoda vrátí velikost výstupních vektorů odhadu výkonové spektrální hustoty.
     * \return Hodnota velikosti výstupních vektorů odhadu výkonové spektrální hustoty.
     */
    int espdSize() const;

    /*!
     * \brief maxSegmentSize Metoda vrátí maximální velikost akceptovatelných vstupních vektorů.
     * \return Maximální velikost akceptovatelných vstupních vektorů.
     */
    int maxInputSize() const;

    /*!
     * \brief computeEspd Metoda provede samotnou dopřednou Fourierovu transformaci a následný výpočet
     *                    magnitud jednotlivých výsledných komplexních čísel. Tyto magnitudy jsou počítány
     *                    pomocí Euklidovské normy a předstvají tzv. odhad výkonové spektrální hustoty
     *                    vstupního vektoru. V případě, že vstupní vektor je větší než m_maxInputSize,
     *                    metoda vrací prázdný QVector.
     * \param input Vektor, který obsahuje vzorky segmentu, z něhož je počítána diskrétní F. transformace.
     * \return Vektor, který reprezentuje odhad výkonové spektrální hustoty daného vektoru nebo prázdný
     *         vektor při chybě.
     */
    QVector<float> computeEspd(const QVector<float> &input);

    /*!
     * \brief computeEspd Přetížená metoda. Provede diskréní Fourierovu transformaci a následný výpočet magnitud
     *                    výsledných komplexních koeficientů. Tyto magnitudy jsou počítány pomocí Euklidovské
     *                    normy a představují tzv. odhad výkonové spektrální hustoty vstupního vektoru. V případě,
     *                    že je vstupní vektor větší než m_maxInputSize, metoda vrací prázdný QVector. Pokud počet
     *                    prealokovaných prvků ve výstupním vektoru nebude roven m_espdSize, bude realokován dle
     *                    potřeby.
     * \param input Vektor, který obsahuje vzorky segmentu, z něhož je počítána disktréní F. transformace.
     * \param espd Reference na vektor, kam se bude zapisovat výsledný odhad výk. spek. hus. vstupního vektoru.
     * \return True, pokud výpočet proběhne v pořádku, jinak false.
     */
    bool computeEspd(const QVector<float> &input, QVector<float> &espd);

    /*!
     * \brief realFft Metoda provádí výpočet komplexních koeficientů Fourierovy transformace.
     * \param input Vstupní vektor signálu.
     * \return Vektor komplexních váhových koeficientů.
     */
    QVector<kiss_fft_cpx> realFFt(const QVector<float> &input);

    /*!
     * \brief realFFt Přetížená metoda. Metoda provádí výpočet komplexních koeficientů Fourierovy transformace.
     *                Výsledné váhové koeficienty zapisuje do výstupního vektoru. Pokud je velikost výstupního vektoru
     *                jiná než m_espdSize, bude vektor realokován. Pokud je vstupní vektor příliš velký nebo neobsahuje
     *                žádné prvky, výstupní vektor bude vyprázdněn.
     * \param input Reference na vstupní vektor.
     * \param cpx Ukazatel na vektor, do kterého budou vloženy výsledné váhové komplexní koeficienty.
     * \return True, pokud dopředná Fourierova transformace proběhla v pořádku, jinak false.
     */
    bool realFFt(const QVector<float> &input, QVector<kiss_fft_cpx> &cpx);

    /*!
     * \brief invRealFFT Metoda provádí inverzi diskrétní Fourierovy transformace pomocí daného vektoru váhových
     *                   koeficientů. Prvky vypočtené inverze jsou děleny hodnotou 1/N (viz inverse FFT scaling factor).
     * \param cpx Vstupní vektor komplexních váhových koeficientů.
     * \return Vektor vzorků signálu v časové doméně.
     */
    QVector<float> invRealFFT(const QVector<kiss_fft_cpx> &cpx);

    /*!
     * \brief invRealFFT Přetížená metoda. Metoda provádí inverzi diskrétní Fourierovy transformace pomocí daného vektoru váhových
     *                   koeficientů. Prvky vypočtené inverze jsou děleny hodnotou 1/N (viz inverse FFT scaling factor). Výsledné
     *                   vzorky signálu v čase se ukládají do výstupního vektoru daného referencí. Pokud se velikost vektoru cpx
     *                   liší od hodnoty m_espdSize, metoda vrací hodnotu false. Pokud výstupní vektor není předalokovaný
     *                   na velikost m_maxInputSize, metoda jej automaticky realokuje.
     * \param cpx Reference na vektor, který obsahuje váhové koeficienty.
     * \param output Reference na vektor, do kterého budou vloženy výsledné vzorky segmentu.
     * \return True, pokud Fourierova transformace proběhla v pořádku, jinak false.
     */
    bool invRealFFT(const QVector<kiss_fft_cpx> &cpx, QVector<float> &output);

private:
    int m_maxInputSize;         //!< Maximální velikost vstupních vektorů (mocnina čísla 2).
    int m_espdSize;             //!< Velikost výstupních vektorů odhadu výkonové spektrální hustoty.
    kiss_fftr_cfg m_fftCfg;     //!< Struktura knihovny kissFFT potřebná pro výpočet FFT.
    kiss_fftr_cfg m_ifftCft;    //!< Struktura knihovny kissFFT potřebná pro výpočet inverze FFT.

    /*!
     * \brief getMaxSegmentSize Je nutné, aby počet vstupních prvků byl mocninou čísla 2. Tato metoda pomocí zadané
     *                          předpokládané velikosti jednolivých segmentů vypočítá nejbližší další mocninu čísla
     *                          2, která se tak stane maximální velikostí jednotlivých segmentů.
     * \return True, pokud inicializace proběhla v pořádku, či nikoliv.
     */
    bool initMaxSegmentSize(int estSegSize);

    /*!
     * \brief initEspdSize Metoda pomocí hodnoty m_maxSegmentSize vypočítá velikost vektorů odhadů výsledných výkonových
     *                     spektrálních hustot podle vztahu, který je blíže popsán v dokumentaci knihovny KissFFT.
     */
    void initEspdSize();

    /*!
     * \brief initKissFFT Metoda podle maximální velikosti akceptovatelných vstupních segmentů alokuje potřebné zdroje
     *                    pro knihovnu KissFFT. Pokud se alokace nepodaří, metoda vrací false a emituje patřičný error signál.
     * \return True, pokud alokace proběhla v pořádku, jinak false.
     */
    bool initKissFFT();

    /*!
     * \brief supplementSegment Tato metoda je volána v případě, že velikost vstupního segmentu je menší
     *                          než hodnota m_maxSegmentSize. V takovém případě metoda vytvoří nový
     *                          vektor, do kterého zkopíruje prvky vstupního vektoru. Takto vytvořený vektor
     *                          doplní čísly nula do počtu m_maxSegmentSize.
     * \param input Vstupní segment akustického signálu.
     * \return Zkopírovaný vektor, který je doplněný nulami na délku m_maxSegmentSize.
     */
    QVector<float> supplementSegment(const QVector<float> &input);

    /*!
     * \brief euclidNorm Přetížená metoda. Podle zadaného pole komplexních koeficientů provede výpočet jejich magnitud
     *                   pomocí Euclidovy normy (l2). Tyto magnitudy vloží do vektoru daném referencí. Výstupní vektor
     *                   je dle potřeby relokován.
     * \param cpx Vektor komplexních váhových koeficientů, u kterých jsou počítány jejich magnitudy.
     * \param espd Výstupní vektor magnitud komplexních koeficientů, tj. odhad výkonové spektrální hustoty.
     */
    bool euclidNorm(const QVector<kiss_fft_cpx> &cpx, QVector<float> &espd);
};

#endif
