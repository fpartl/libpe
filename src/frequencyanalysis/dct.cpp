#include "dct.h"

float DCT::dct(QVector<float> xi, int mth) {
    float result = 0.0f;

    for (int n = 0; n < xi.size(); n++) {
        double incos = ((M_PI * static_cast<double>(mth)) / static_cast<double>(xi.size())) * (static_cast<double>(n) - 0.5);

        result += xi[n] * static_cast<float>(qCos(incos));
    }

    return result;
}

float DCT::idct(QVector<float> xi, int mth) {
    float result = 0.0f;

    for (int n = 1; n < xi.size(); n++) {
        double incos = (M_PI / static_cast<double>(xi.size())) * static_cast<double>(n) * (static_cast<double>(mth) - 0.5);

        result += xi[n] * static_cast<float>(qCos(incos));
    }

    result += (0.5f * xi[0]);
    result *= (2.0f / static_cast<float>(xi.size())); /* škálovací faktor */

    return result;
}
