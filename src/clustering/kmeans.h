#ifndef KMEANS_H
#define KMEANS_H

#include <functional>
#include <limits>

#include <QVector>
#include <QRandomGenerator>

#include "../metrics/metrics.h"

/*!
 * \brief Třída KMeans
 *
 * Tato třída je generickou implementací shlukovacího algoritmu K-means. Byla vytvořena, protože jsem
 * na internetu žádnou pěknou generickou implementaci nenašel, a alespoň jsem si zopakoval K-means ke
 * státnici. Třída je šablonovaná, takže body, které budou rozdělovány do shluků, můžou být libovolné
 * dimenze a jejich složky libovolného typu.
 */
template<class T>
class KMeans {
public:
    /*!
     * \brief Point Definice bodu jako n-dimenzionálního vektoru - pouze pro zvýšení přehlednosti kódu.
     */
    using Point = QVector<T>;

    /*!
     * \brief Struktura Cluster
     *
     * Struktura představuje shluk bodů, který je dán centroidem a vektorem iterátorů (referencí) na
     * body v něm obsažené. Dále struktura obsahuje sumu vzdáleností od centroidu k bodům (chybu).
     */
    struct Cluster {
        QVector<typename QVector<Point>::const_iterator> points;
                                //!< Konstatní iterátory, které značí příslušnost vzorku ke shluku.
        T totalDistance = 0;    //!< Suma vzdáleností bodů shluku od centroidu.
        Point centroid;         //!< Pozice centroidu shluku.
    };

    /*!
     * \brief Struktura Result
     *
     * Struktura obsahuje výsledky provádění algoritmu K-means. Obsahuje tedy vektor nalezených
     * shluků (instance struktury Cluster), sumu vzdáleností všech bodů od jim přidružených centroidů
     * a nakonec počet iterací, po kterých se pozice centroidů již neměnily.
     */
    struct Result {
        QVector<Cluster> clusters;  //!< Vektor nalezených shluků.
        T totalDistance = 0;        //!< Suma vzdáleností bodů shluků od jim přidruženým centroidům.
        int iterations = 0;         //!< Počet iterací, po kterých algoritmus shlukování skončil.
    };

    /*!
     * \brief KMeans Konstruktor třídy, který přijímá metriku zvolenou k určení vzdáleností mezi body.
     * \param metric Metrika zvolená pro měření vzdálenosti mezi dvěma body. Výchozím parametrem je
     *               prostá Euklidovská vzdálenost. Je možné dodat vlastní implementaci metriky
     *               nebo vybrat jednu z připravených a otestovaných implementací v metrics/metrics.h.
     */
    explicit KMeans(std::function<T(const Point&, const Point&)> metric = &Metrics::euclDistN<T>);

    /*!
     * \brief fit Tato metoda spoustí algoritmus K-means nad vektorem bodů data, kde bude určovat pozice
     *            centroidů, jejichž počet a počáteční umístění jsou dány vektorem centroids.
     * \param data Vektor bodů, nad kterými bude prováděna shluková analýza.
     * \param centroids Vektor počátečních pozic centroidů shluků. Jejich počet udává i počet hledaných
     *                  shluků.
     * \return Výsledek algoritmu K-means.
     */
    Result fit(const QVector<Point>& data, const QVector<Point>& centroids) const;

    /*!
     * \brief fit Metoda spustí algoritmus K-means nad vektorem bodů data, kde bude určovat pozice centroidů,
     *            jejichž počet je určen parametrem k. Počáteční umístění centroidů je určeno náhodným výběrem
     *            s rovnoměrným rozdělením pravděpodobnosti z vektoru analyzovaných bodů.
     * \param data Vektor bodů, nad kterými bude prováděna shluková analýza.
     * \param k Počet hledaných shluků.
     * \return Výsledek algoritmu K-means.
     */
    Result fit(const QVector<Point>& data, int k) const;

    /*!
     * \brief fitpp Metoda spustí algoritmus K-means nad vektorem bodů data, kde bude určovat pozice centroidů,
     *              jejichž počet je určen porametrem k. Počáteční umístění centroidů je určeno dle algoritmu
     *              K-means++ (viz internetová dokumentace). Tento způsob analýzy se jeví jako nejlepší.
     * \param data Vektor bodů, nad kterými bude prováděna shluková analýza.
     * \param k Počet hledaných shluků.
     * \param firstCentroid První z centroidů při inicializaci. Pokud bude index menší než nula, zvolí se centroid
     *                      náhodně podle generátoru pseudonáhodných čísel s rovnoměrným rozdělením ppsti.
     * \return Výsledek algoritmu K-means.
     */
    Result fitpp(const QVector<Point>& data, int k, int firstCentroid = -1) const;

    /*!
     * \brief clustrify Pomocí této metody je možné určit, do jakého ze zjištěných shluků bude zařazen
     *                  neznámý bod point. Metoda rovněž může vrátit konkrétní vzdálenost k onomu
     *                  shluku.
     * \param result Výsledek algoritmu K-means, který obsahuje i rozložení shluků.
     * \param point Neznámý bod, který má být přiřazen do jednoho ze shluků.
     * \param distance Ukazatel na paměť, kam bude uložen údaj o vzdálenosti k nejbližšímu centroidu.
     * \return Iterátor do pole shluků, který značí příslušnost bodu ke shluku.
     */
    typename QVector<Cluster>::const_iterator clustrify(const Result& result, const Point& point,
                                                        T* distance = nullptr);

    /*!
     * \brief clustrify Pomocí této metody je možné určit, ke kterému centroidu bude přiřazen neznámý bod
     *                  point. Metoda rovněž může vrátit konkrétní vzdálenost k onomu centroidu.
     * \param centroids Vektor centroidů.
     * \param point Neznámý bod, který má být přiřazen k jednomu z centroidů.
     * \param distance Ukazatel na paměť, kam bude uložen údaj o vzdálenosti k nejbližšímu centroidu.
     * \param metric Metrika užitá při určování shluků algoritmem K-Means.
     * \return Reference na vybraný centroid.
     */
    static const typename KMeans<T>::Point
        clustrify(const QVector<Point>& centroids, const Point& point, T* distance = nullptr,
                  std::function<T(const Point&, const Point&)> metric = &Metrics::euclDistN<T>);

private:
    std::function<double(const Point&, const Point&)> m_metric;   //!< Metrika pro měření vzdálenosti mezi dvěma body.

    /*!
     * \brief MAX_ITERATIONS Maximální počet iterací algoritmu K-means.
     */
    static constexpr int MAX_ITERATIONS = 10000;

    /*!
     * \brief fit Metoda provádí algoritmus K-means nad vektorem bodů data, kde bude určovat pozice centroidů,
     *            jejichž počet je určen vektorem centroidů v instanci struktury Result, která je druhým
     *            parametrem metody. Na konci analýzy jsou v této instanci doplněny i informace o celkových
     *            vzdálenostech (chybách).
     * \param data Vektor bodů, nad kterými bude prováděna shluková analýza.
     * \param result Při volání obsahuje pouze počáteční pozice centroidů, které jsou v průběhu analýzy
     *               přesouvány, takže na konci vyhodnocování metody obsahují výsledek analýzy.
     */
    void fit(const QVector<Point>& data, Result& res) const;

    /*!
     * \brief nearestCluster Metoda na základně daného bodu určí centroid, který je mu nejblíže. Centroidy
     *                       jsou dány vektorem existujících shluků (centroidů). Metoda kromě vrácení iterátoru,
     *                       který označuje nejblížší centroid, umí informovat i o vzdálenosti mezi bodem
     *                       a jemu nejbližším centroidem.
     * \param point Bod, pro který se hledá nejbližší centroid (shluk, do které patří).
     * \param clusters Vektor dostupných shluků (centroidů).
     * \param minDistance Ukazatel, pomocí kterého může metoda předat vzdálenost k nejbližšímu centroidu.
     * \return Iterátor, který ve vektoru clusters označuje nejbližší centroid k bodu point.
     */
    typename QVector<Cluster>::iterator nearestCluster(const Point& point,
                                                       QVector<Cluster>& clusters,
                                                       T* minDistance = nullptr) const;

    /*!
     * \brief assignPoints Metoda projde všechny body určené ke shlukovací analýze a rozdělí je do shluků podle
     *                     jejich vzdálenosti ke všem centroidům.
     * \param data Vektor bodů, které budou rozděleny mezi shluky.
     * \param clusters Shluky (pozice centroidů), do kterých budou body rozděleny.
     */
    inline void assignPoints(const QVector<Point>& data, QVector<Cluster>& clusters) const;

    /*!
     * \brief moveCentroids Metoda projde zadané shluky a podle bodů v nich obsažených přesune centroidy do
     *                      střední hodnoty shluku (aritmetický průměr všech vektorů).
     * \param clusters Vektor shluků, jejichž centroidy budou posunuty.
     * \return True, pokud se pozice centroidů po přesunutí změnily, jinak false - konec algoritmu.
     */
    bool moveCentroids(QVector<Cluster>& clusters) const;

    /*!
     * \brief clearClusters Metoda provede odstranění všech bodů ze zadaných shluků. Je tedy možné body v další
     *                      iteraci opět přerozdělit podle nových poloh centroidů.
     * \param clusters Vektor shluků, ze kterých budou odstraněny body.
     */
    inline void clearClusters(QVector<Cluster>& clusters) const;

    /*!
     * \brief calcTotalDists Metoda pro každý shluk nasčítá sumu vzdáleností od jeho centroidu ke všem shlukům.
     *                       Tyto součty následně sečte do celkové chyby shlukové analýzy.
     * \param result Instance výsledku shlukovací analýzy, kde jsou již pozice centroidů finální, tj. je rovněž
     *               známo, který bod patří do kterého shluku.
     */
    void calcTotalDists(Result& result) const;
};

template<class T>
KMeans<T>::KMeans(std::function<T(const Point&, const Point&)> metric)
    : m_metric(metric)
{ }

template<class T>
typename KMeans<T>::Result KMeans<T>::fit(const QVector<Point> &data, const QVector<Point> &centroids) const {
    if (centroids.isEmpty() || centroids.size() > data.size())
        return Result();

    Result res = { QVector<Cluster>(centroids.size()), 0, 0 };
    for (auto ce = centroids.begin(), cl = res.clusters.begin(); ce != centroids.end(); ++ce, ++cl)
        (*cl).centroid = *ce;

    fit(data, res);
    return res;
}

template<class T>
typename KMeans<T>::Result KMeans<T>::fit(const QVector<Point> &data, int k) const {
    if (k <= 0 || k > data.size())
        return Result();

    QRandomGenerator gen = QRandomGenerator::securelySeeded();
    QVector<Cluster> clusters(k);
    QVector<int> centroidPositions;
    centroidPositions.reserve(k);

    for (auto& cls : clusters) {
        int centroidPos = gen.bounded(data.size());

        if (!centroidPositions.contains(centroidPos)) {
            cls.centroid = data[centroidPos];
            centroidPositions.append(centroidPos);
        }
    }

    Result res = { clusters, 0, 0 };
    fit(data, res);
    return res;
}

template<class T>
typename KMeans<T>::Result KMeans<T>::fitpp(const QVector<Point> &data, int k, int firstCentroid) const {
    if (k <= 0 || k > data.size())
        return Result();

    QRandomGenerator gen = QRandomGenerator::securelySeeded();

    QVector<Cluster> clusters(1); // Ten první je jasný -> náhodně generovaný z bodů.
    clusters.reserve(k);

    clusters[0].centroid = (firstCentroid < 0 || firstCentroid >= data.size())
                                ? clusters[0].centroid = data[gen.bounded(data.size())]
                                : clusters[0].centroid = data[firstCentroid];

    QVector<T> dists(data.size());
    T distSum;

    do {
        distSum = 0;

        for (auto x = data.begin(), d = dists.begin(); x != data.end(); ++x, ++d) {
            nearestCluster(*x, clusters, d);
            distSum += *d;
        }

        distSum = gen.bounded(distSum);

        for (auto x = data.begin(), d = dists.begin(); x != data.end(); ++x, ++d) {
            if ((distSum -= *d) > 0)
                continue;

            clusters.append({ QVector<typename QVector<QVector<T>>::const_iterator>(), 0, *x });
            break;
        }
    } while (clusters.size() < k);

    Result res = { clusters, 0, 0 };
    fit(data, res);
    return res;
}

template<class T>
typename QVector<typename KMeans<T>::Cluster>::const_iterator
    KMeans<T>::clustrify(const Result &result, const Point &point, T *distance)
{
    return nearestCluster(point, const_cast<KMeans::Result&>(result).clusters, distance);
}

template<class T>
const typename KMeans<T>::Point
    KMeans<T>::clustrify(const QVector<Point> &centroids, const Point &point, T *distance,
                         std::function<T(const Point&, const Point&)> metric)
{
    QVector<Cluster> clusters;
    clusters.reserve(centroids.size());

    for (const Point& c : centroids)
        clusters.append({ QVector<typename QVector<Point>::const_iterator>(), 0, c });

    KMeans kmeans(metric);
    typename QVector<Cluster>::iterator nearestCluster = kmeans.nearestCluster(point, clusters, distance);

    return nearestCluster->centroid;
}

template<class T>
void KMeans<T>::fit(const QVector<Point> &data, Result &res) const {
    for (res.iterations = 0; res.iterations < MAX_ITERATIONS; ++res.iterations) {
        // Přidružím všechny body k jejich nejbližším centroidům.
        assignPoints(data, res.clusters);

        // Posunu centroidy všech shluků a pokud se pozice centroidů nezmění, ukončím algoritmus.
        if (moveCentroids(res.clusters))
            clearClusters(res.clusters);
        else break;
    }

    calcTotalDists(res);
}

template<class T>
typename QVector<typename KMeans<T>::Cluster>::iterator
    KMeans<T>::nearestCluster(const Point &point, QVector<Cluster> &clusters, T* minDistance) const
{
    typename QVector<Cluster>::iterator nearestCls = clusters.begin();
    T nearestDist = std::numeric_limits<T>::max();

    for (auto cl = clusters.begin(); cl != clusters.end(); ++cl) {
        T dist = m_metric(point, cl->centroid);

        if (dist < nearestDist) {
            nearestCls = cl;
            nearestDist = dist;
        }
    }

    if (minDistance)
        *minDistance = nearestDist;

    return nearestCls;
}

template<class T>
void KMeans<T>::assignPoints(const QVector<Point> &data, QVector<Cluster> &clusters) const {
    for (auto x = data.constBegin(); x != data.constEnd(); ++x)
        nearestCluster(*x, clusters)->points.append(x);
}

template<class T>
bool KMeans<T>::moveCentroids(QVector<Cluster>& clusters) const {
    bool centroidsMoved = false;

    for (auto cl = clusters.begin(); cl != clusters.end(); ++cl) {
        for (int i = 0; i < cl->centroid.size(); ++i) {
            T mean = 0;

            for (const auto &point : cl->points)
                mean += (*point)[i];

            mean /= cl->points.size();

            if (mean != cl->centroid[i]) {
                cl->centroid[i] = mean;
                centroidsMoved = true;
            }
        }
    }

    return centroidsMoved;
}

template<class T>
void KMeans<T>::clearClusters(QVector<Cluster>& clusters) const {
    for (auto &cl : clusters)
        cl.points.clear();
}

template<class T>
void KMeans<T>::calcTotalDists(Result& result) const {
    for (auto &cl : result.clusters) {
        for (const auto &point : cl.points)
            cl.totalDistance += m_metric(cl.centroid, *point);

        result.totalDistance += cl.totalDistance;
    }
}

#endif


