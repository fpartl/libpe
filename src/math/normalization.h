#ifndef NORMALIZATION_H
#define NORMALIZATION_H

#include <QVector>

/*!
 * \brief Třída Normalization
 *
 * Pomocí této třídy je možné provést normalizaci zadaného vektoru. Jde pouze o snahu operace
 * tohoto ražení nějakým způsobem unifikovat a otestovat.
 */
class Normalization {
public:
    /*!
     * \brief normalize Metoda provede normalizaci dat se zadaným maximem.
     * \param data Vektor vzorků, které budou normalizovány.
     * \param max Maximální hodnota v normalizovaném vektoru.
     */
    template <class T>
    static void normalize(QVector<T>& data, const T& max);
};

template<class T>
void Normalization::normalize(QVector<T>& data, const T& max) {
    T dataMax = *std::max_element(data.begin(), data.end());

    std::transform(data.begin(), data.end(), data.begin(),
        [&dataMax, &max] (T i) -> T {
            return (i / dataMax) * max;
        }
    );
}

#endif
