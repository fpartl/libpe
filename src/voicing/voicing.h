#ifndef VOICING_H
#define VOICING_H

#include <QVector>

/*!
 * \brief Třída Voicing
 *
 * Tato třída obsahuje metody, které na základě daných odhadů výkonových spektrálních hustot
 * stanovují, zda se jedná o tzv. znělý či neznělý segment akustického signálu.
 */
class Voicing {
public:
    /*!
     * \brief isVoiced Jednoduchá metoda přejatá od Ing. Kamila Ekšteina, Ph.D.
     * \param s Odhad výkonové spektrální hustoty segmentu.
     * \param fs Vzorkovací frekvence signálu.
     * \param fvm Frekvence, která odděluje hlavní oblast lidské řeči od zbytku odhadu.
     * \return True, pokud je segment znělý, jinak false.
     */
    template <class T>
    static bool isVoiced(const QVector<T>& s, int fs, int fvm = 500);
};

#endif

template<class T>
bool Voicing::isVoiced(const QVector<T> &espd, int fs, int fvm) {
    float e_all = 0.0f, e_voiced = 0.0f;

    int m = (2 * fvm * espd.size()) / fs;

    for (int i = 0; i < espd.size(); ++i) {
        if (i == m)
            e_voiced = e_all;

        e_all += espd[i];
    }

    return (e_all / espd.size()) < (e_voiced / m);
}
