#ifndef VOICEMODEL_H
#define VOICEMODEL_H

#include <QVector>
#include <QtMath>

/*!
 * \brief Třída VoiceModel
 *
 * Pomocí této třídy je možné namodelovat libovolnou formantovou strukturu, která představuje odhad
 * výkonové spektrální hustoty lidského hlasu. Jednotlivé formanty jsou gaussovské křivky, které lze
 * umístit na přesnou frekvenci se zadanou výškou a rozptylem.
 */
template <class T>
class VoiceModel {
public:
    /*!
     * \brief VoiceModel Konstruktor třídy.
     * \param espdSize Počet vzorků odhadu výkonové spektrální hustoty.
     * \param sampFreq Vzorkovací frekvence, která bude pomocí NSK teorému převedena na maximální
     *                 frekvenci v signálu.
     */
    explicit VoiceModel(int espdSize, int sampFreq);

    /*!
     * \brief model Nekonstantní reference na zkontruovaný model odhadu výkonové spektrální hustoty
     *              lidské řeči. Reference je nekonstantní, aby bylo možné například model normalizovat.
     *              Pozor ale v případě změny počtu vzorků! V tom případě není chování objektu této
     *              třídy definováno.
     * \return Reference na vektor vzorků modelu.
     */
    QVector<T>& model();

    /*!
     * \brief addFormant Metoda, kterou je možné přidat další gaussovskou křivku do spektra.
     * \param freq Frekvence (střední hodnota) formantu.
     * \param sigma Rozptyl formantu ("šířka" křivky).
     * \param height Výška křivky.
     */
    void addFormant(int freq, T sigma, T height);

private:
    QVector<T> m_model;     //!< Vektor vzorků modelu odhadu výkonové spektrální hustoty lidské řeči.
    int m_maxFreq;          //!< Maximální frekvence v signálu (polovina vzorkovací).

    /*!
     * \brief hzToEspd Podle zadané maximální frekvence tato metoda převádí zadanou frekvenci na index
     *                 v odhadu výkonové spektrální hustoty.
     * \param hz Frekvence v Hz.
     * \return Odpovídající frekvence v odhadu výkonové spektrální hustoty.
     */
    inline int hzToBin(int hz);
};

template<class T>
VoiceModel<T>::VoiceModel(int espdSize, int sampFreq)
    : m_model(espdSize),
      m_maxFreq(sampFreq / 2) // Shannonův–Nyquistův–Kotělnikovův teorém
{
    // Toto je nastíněno jako jeden z možných modelů.
    //addFormant(180, 2, 0.6);
    //addFormant(360, 3, 0.8);
    //addFormant(500, 3, 1);
}

template<class T>
QVector<T> &VoiceModel<T>::model() {
    return m_model;
}

template<class T>
void VoiceModel<T>::addFormant(int freq, T sigma, T height) {
    freq = hzToBin(freq);

    for (int f = 0; f < m_model.size(); ++f) {
        double exp = qPow(static_cast<double>(f - freq), 2.0) / (2.0 * qPow(sigma, 2.0));
        double g_f = height * qPow(M_E, -exp);

        m_model[f] += static_cast<float>(g_f);
    }
}

template<class T>
int VoiceModel<T>::hzToBin(int hz) {
    return hz * m_model.size() / m_maxFreq;
}

#endif
