#ifndef NVAD_H
#define NVAD_H

#include <QVector>
#include <QPair>

#include "../math/statistics.h"

/*!
 * \brief Třída NaiveVoiceDetector
 *
 * Metody této třídy podle průměrné energie signálu odstraňují segmenty signálu, které neobsahují důležitý
 * signál. Třída nabízí metodu pro odstranění všech segmentů signálu s energií pod zadaný práh. Dále je možné
 * signál pouze ořezat ze začátku a konce.
 */
template <class SampleT>
class NaiveVoiceDetector {
public:
    /*!
     * \brief NaiveVoiceDetector Konstruktor třídy.
     * \param treshold Práh, při kterém budou segmenty vypuštěny. Přesněji energie segmentu < průměrná
     *                 energie segmentů * m_treshold => vyhodit!
     */
    explicit NaiveVoiceDetector(double treshold = 0.1);

    /*!
     * \brief getVoiceSegments Metoda vypočítá průměrnou energii každého ze segmentů. Tento průměr je následně
     *                         vynásoben hodnotou m_treshold, čímž vzniká konkrétní práh. Výsledkem je vektor
     *                         iterátorů, které v analyzovaném vektoru segmentů ukazují na ty, které mají
     *                         energii větší než práh.
     * \param segments Vektor segmentů signálu.
     * \return Vektor iterátorů, které označují segmenty s energií větší než je práh.
     */
    QVector<typename QVector<QVector<SampleT>>::iterator>
        getVoiceSegments(QVector<QVector<SampleT>>& segments);

private:
    /* Následující funkce obsahuje chybu! Nevím v čem, ale podle této funkce jsou všechny segmenty pod prahem. */
    /*!
     * \brief trimVoice Metoda prochází signál od začátku, dokud nenarazí na množinu "s" segmentů, které mají
     *                  energii větší než práh. Stejný postup je proveden od konce signálu. Zbylé segmenty,
     *                  které obsahují důležitý signál, jsou ponechány a je vrácen pár segmentů, které jsou
     *                  hraničními segmenty extrahovaného signálu.
     * \param segments Vektor segmentů signálu.
     * \param boundSize Počet segmentů souvislého bloku, které označují začátek, resp. konec důležité části
     *                  signálu.
     * \return Pár iterátorů, které označují první a poslední segment extrahovaného signálu.
     */
    QPair<typename QVector<QVector<SampleT>>::iterator, typename QVector<QVector<SampleT>>::iterator>
        trimVoice(QVector<QVector<SampleT>> &segments, int boundSize = 2);

private:
    double m_treshold;  //!< Práh, při kterém budou segmenty vypuštěny.
                        //!< energie segmentu < průměrná energie segmentů * m_treshold => vyhodit!

    /*!
     * \brief getEnergies Metoda pomocí metody segmentEnergy vypočítá energie všech signálů, které vrátí.
     *                    V případě uvedení ukazatele minEnergy vypočítá i hodnotu prahu, která je dána
     *                    součinem průměrné energie a hodnoty m_treshold;
     * \param segments Vektor segmentů signálu.
     * \param minEnergy Ukazatel na paměť, kam je zapsána hodnota prahu při současném nastavení m_treshold.
     * \return Vektor energií segmentů signálů.
     */
    QVector<double> getEnergies(const QVector<QVector<SampleT>> &segments, double *minEnergy);

    /*!
     * \brief segmentEnergy Metoda spočítá energii segmentu signálu.
     * \param segment Segment signálu.
     * \return Energie segmentu.
     */
    double segmentEnergy(const QVector<SampleT>& segment);
};

template<class SampleT>
NaiveVoiceDetector<SampleT>::NaiveVoiceDetector(double treshold) : m_treshold(treshold) { }

template<class SampleT>
QVector<typename QVector<QVector<SampleT>>::iterator>
    NaiveVoiceDetector<SampleT>::getVoiceSegments(QVector<QVector<SampleT>> &segments)
{
    double minEnergy;
    QVector<double> energies = getEnergies(segments, &minEnergy);

    QVector<typename QVector<QVector<SampleT>>::iterator> voicedSegments;
    for (auto s = segments.begin(), e = energies.begin(); s != segments.end(); ++s, ++e) {
        if (*e >= minEnergy)
            voicedSegments.append(s);
    }

    return voicedSegments;
}

template<class SampleT>
QPair<typename QVector<QVector<SampleT>>::iterator, typename QVector<QVector<SampleT>>::iterator>
    NaiveVoiceDetector<SampleT>::trimVoice(QVector<QVector<SampleT> > &segments, int boundSize)
{
    double minEnergy;
    QVector<double> energies = getEnergies(segments, &minEnergy);

    int biggestBlock = 0;
    QPair<typename QVector<QVector<SampleT>>::iterator, typename QVector<QVector<SampleT>>::iterator> bounds;
    for (auto s = segments.begin(), e = energies.begin(); s != segments.end(); ++s, ++e) {
        biggestBlock = (*e >= minEnergy) ? biggestBlock + 1 : 0;

        if (biggestBlock >= boundSize) {
            bounds.first = s - boundSize;
            break;
        }
    }

    biggestBlock = 0;
    for (auto s = segments.rend(), e = energies.rend(); s != segments.rbegin(); --s, --e) {
        biggestBlock = (*e >= minEnergy) ? biggestBlock + 1 : 0;

        if (biggestBlock >= boundSize) {
            bounds.second = (s + boundSize + 1).base();
            break;
        }
    }

    return bounds;
}

template<class SampleT>
QVector<double> NaiveVoiceDetector<SampleT>::
    getEnergies(const QVector<QVector<SampleT>> &segments, double *minEnergy)
{
    QVector<double> energies(segments.size());

    for (auto s = segments.begin(), e = energies.begin(); s != segments.end(); ++s, ++e)
        *e = segmentEnergy(*s);

    *minEnergy = Statistics::arithmeticMean(energies) * m_treshold;
    return energies;
}

template<class SampleT>
double NaiveVoiceDetector<SampleT>::segmentEnergy(const QVector<SampleT> &segment) {
    double energy = 0;

    for (const SampleT& x : segment)
        energy += std::pow(x, 2);

    return energy;
}

#endif

