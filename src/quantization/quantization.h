#ifndef QUANTIZATION_H
#define QUANTIZATION_H

#include <QVector>

/*!
 * \brief Třída Quantization
 *
 * Třída představuje rozhraní obecného kvantizačního nástroje. Předepisuje metody pro
 * kvantizaci jediné hodnoty i celých vektorů hodnot. Třída je šablonová, takže je
 * možné kvantizovat hodnoty typu float, double, int, atd.
 */
template <class T>
class Quantization {
public:
    /*!
     * \brief quantize Metoda provede kvantizaci jediné hodnoty. Kvantizovanou hodnotu vrátí.
     * \param value Hodnota, která bude kvantizována.
     * \param step Velikost kvantizační úrovně.
     * \return Kvantizovaná hodnota.
     */
    T quantize(T value, T step) const;

    /*!
     * \brief quantize Metoda provede kvantizaci celého vektoru hodnot. Původní hodnoty vektoru
     *                 budou nahrazeny kvantizovanými.
     * \param vector Vektor, jehož hodnoty budou kvantizovány.
     */
    virtual void quantize(QVector<T>& vector) const = 0;
};

template<class T>
T Quantization<T>::quantize(T value, T step) const {
    if (step <= 0)
        return T();

    return static_cast<T>(qRound(static_cast<double>(value) / static_cast<double>(step))) * step;
}

#endif
