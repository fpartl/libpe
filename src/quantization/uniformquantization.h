#ifndef UNIFORMQUANTIZATION_H
#define UNIFORMQUANTIZATION_H

#include "quantization.h"

/*!
 * \brief Třída UniformQuantization
 *
 * Třída představuje uniformní kvantizační nástroj.
 */
template <class T>
class UniformQuantization : public Quantization<T> {
public:
    /*!
     * \brief UniformQuantization Konstruktor třídy.
     * \param step Krok pro kvantizaci hodnot.
     */
    explicit UniformQuantization(T step);

    /*!
     * \brief quantize Metoda provede kvantizaci celého vektoru hodnot. Původní hodnoty vektoru
     *                 budou nahrazeny kvantizovanými.
     * \param vector Vektor, jehož hodnoty budou kvantizovány.
     */
    void quantize(QVector<T> &vector) const override;

    /*!
     * \brief step Metoda vrátí nastavený kvantizační krok.
     * \return Kvantizační krok.
     */
    T step() const;

private:
    T m_step;       //!< Kvantizační krok.
};

template<class T>
UniformQuantization<T>::UniformQuantization(T step) : m_step(step) {
    Q_ASSERT(step > 0);
}

template<class T>
void UniformQuantization<T>::quantize(QVector<T> &vector) const {
    for (int i = 0; i < vector.size(); i++)
        vector[i] = Quantization<T>::quantize(vector[i], m_step);
}

template<class T>
T UniformQuantization<T>::step() const {
    return m_step;
}

#endif
