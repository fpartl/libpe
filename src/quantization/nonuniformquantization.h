#ifndef NONUNIFORMQUANTIZATION_H
#define NONUNIFORMQUANTIZATION_H

#include <QVector>
#include <QtDebug>

#include "quantization.h"

/*!
 * \brief Třída NonUniformQuantization
 *
 * Třída představuje neuniformní kvantizační nástroj. Podle vektoru zadaných kvantizačních úrovní
 * kvantizuje další vstupní vektory.
 */
template <class T>
class NonUniformQuantization : public Quantization<T> {
public:
    /*!
     * \brief NonUniformQuantization Konstruktor třídy.
     * \param steps Kvantizační úrovně.
     */
    explicit NonUniformQuantization(const QVector<T> steps);

    ~NonUniformQuantization() {}

    /*!
     * \brief quantize Metoda provede kvantizaci zadaného vektoru hodnot. Hodnoty zadaného vektoru budou
     *                 nahrazeny kvantizovanými. Pokud délka vstupního vektoru bude různá od počtu zadaných
     *                 kvantizačních kroků, metoda neprovede žádnou akci.
     * \param vector Hodnoty určené ke kvantizaci.
     */
    void quantize(QVector<T> &vector) override;

    /*!
     * \brief steps Metoda vrátí vektor nastavených kvantizačních úrovní.
     * \return Nastavené kvantizační úrovně.
     */
    QVector<T> steps() const;

private:
    QVector<T> m_steps;     //!< Kvantizační úrovně.
};

template<class T>
NonUniformQuantization<T>::NonUniformQuantization(const QVector<T> steps) : m_steps(steps){
    if (m_steps.isEmpty())
        qDebug() << "Vektor kvantizacnich kroku je prazny.";

    for (const T& i : m_steps) {
        if (i <= 0)
            qDebug() << "Step musi byt > 0.";
    }
}

template<class T>
void NonUniformQuantization<T>::quantize(QVector<T> &vector) {
    if (vector.size() != m_steps.size()) {
        qDebug() << "NonUniformQuantization: vector.size() != m_steps.size()";
        return;
    }

    for (int i = 0; i < vector.size(); i++)
        vector[i] = Quantization<T>::quantize(vector[i], m_steps[i]);
}

template<class T>
QVector<T> NonUniformQuantization<T>::steps() const {
    return m_steps;
}


#endif
